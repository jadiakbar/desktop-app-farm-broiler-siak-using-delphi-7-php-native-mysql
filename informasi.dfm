object Frame15: TFrame15
  Left = 0
  Top = 0
  Width = 969
  Height = 313
  TabOrder = 0
  object sLabelFX1: TsLabelFX
    Left = 8
    Top = 8
    Width = 70
    Height = 22
    Caption = 'Informasi'
    ParentFont = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold, fsUnderline]
    Angle = 0
    Shadow.OffsetKeeper.LeftTop = -2
    Shadow.OffsetKeeper.RightBottom = 4
  end
  object sLabelFX2: TsLabelFX
    Left = 72
    Top = 40
    Width = 881
    Height = 19
    Caption = 
      'Program merupakan hasil implementasi Penerapan Metode Indeks Mus' +
      'im dan Metode Least Square Untuk Meramalkan Persediaan dan Penju' +
      'alan Pada Sistem Informasi CV. Farm Broiler'
    Angle = 0
    Shadow.OffsetKeeper.LeftTop = -2
    Shadow.OffsetKeeper.RightBottom = 4
  end
  object sLabelFX3: TsLabelFX
    Left = 8
    Top = 64
    Width = 302
    Height = 19
    Caption = 'di Kabupaten Siak. Program memiliki informasi sebagai berikut :'
    Angle = 0
    Shadow.OffsetKeeper.LeftTop = -2
    Shadow.OffsetKeeper.RightBottom = 4
  end
  object sLabelFX4: TsLabelFX
    Left = 160
    Top = 96
    Width = 29
    Height = 19
    Caption = 'Data'
    Angle = 0
    Shadow.OffsetKeeper.LeftTop = -2
    Shadow.OffsetKeeper.RightBottom = 4
  end
  object sLabelFX5: TsLabelFX
    Left = 160
    Top = 120
    Width = 48
    Height = 19
    Caption = 'Informasi'
    Angle = 0
    Shadow.OffsetKeeper.LeftTop = -2
    Shadow.OffsetKeeper.RightBottom = 4
  end
  object sLabelFX6: TsLabelFX
    Left = 160
    Top = 144
    Width = 45
    Height = 19
    Caption = 'Laporan'
    Angle = 0
    Shadow.OffsetKeeper.LeftTop = -2
    Shadow.OffsetKeeper.RightBottom = 4
  end
  object sLabelFX7: TsLabelFX
    Left = 160
    Top = 168
    Width = 90
    Height = 19
    Caption = 'Sasaran Informasi'
    Angle = 0
    Shadow.OffsetKeeper.LeftTop = -2
    Shadow.OffsetKeeper.RightBottom = 4
  end
  object sLabelFX8: TsLabelFX
    Left = 272
    Top = 96
    Width = 206
    Height = 19
    Caption = ': Agen, Pegawai, Suplier, Gudang, Barang'
    Angle = 0
    Shadow.OffsetKeeper.LeftTop = -2
    Shadow.OffsetKeeper.RightBottom = 4
  end
  object sLabelFX9: TsLabelFX
    Left = 272
    Top = 120
    Width = 582
    Height = 19
    Caption = 
      ': Data Agen, Data Pegawai, Data Suplier, Data Gudang, Data Baran' +
      'g, Data Barang Masuk, Data Barang Keluar dan Stok'
    Angle = 0
    Shadow.OffsetKeeper.LeftTop = -2
    Shadow.OffsetKeeper.RightBottom = 4
  end
  object sLabelFX10: TsLabelFX
    Left = 272
    Top = 144
    Width = 603
    Height = 19
    Caption = 
      ': PrintOut Stok, Data Barang Masuk, Data Barang Keluar dan (Lapo' +
      'ran Barang Masuk dan Barang Keluar pada Microsoft Exel)'
    Angle = 0
    Shadow.OffsetKeeper.LeftTop = -2
    Shadow.OffsetKeeper.RightBottom = 4
  end
  object sLabelFX11: TsLabelFX
    Left = 272
    Top = 168
    Width = 314
    Height = 19
    Caption = ': Stok berupa Diagram PerBarang, Harga Modal dan Keuntungan'
    Angle = 0
    Shadow.OffsetKeeper.LeftTop = -2
    Shadow.OffsetKeeper.RightBottom = 4
  end
  object sLabelFX12: TsLabelFX
    Left = 760
    Top = 288
    Width = 207
    Height = 19
    Caption = 'Tertanda Indra Husada Praharta Hasibuan'
    Angle = 0
    Shadow.OffsetKeeper.LeftTop = -2
    Shadow.OffsetKeeper.RightBottom = 4
  end
end
