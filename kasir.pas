unit kasir;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, DB, ADODB, Buttons, sSpeedButton, Mask, sMaskEdit,
  sCustomComboEdit, sTooledit, StdCtrls, sBitBtn, sEdit, sLabel, Grids,
  DBGrids, sGroupBox, sComboBox;

type
  TFrame14 = class(TFrame)
    sGroupBox2: TsGroupBox;
    DBGrid1: TDBGrid;
    sGroupBox1: TsGroupBox;
    sLabelFX1: TsLabelFX;
    sLabelFX2: TsLabelFX;
    sLabelFX3: TsLabelFX;
    kd_agen: TsEdit;
    nm_agen: TsEdit;
    sBitBtn1: TsBitBtn;
    sBitBtn2: TsBitBtn;
    sBitBtn3: TsBitBtn;
    sBitBtn4: TsBitBtn;
    sGroupBox3: TsGroupBox;
    sSpeedButton1: TsSpeedButton;
    sSpeedButton2: TsSpeedButton;
    sSpeedButton3: TsSpeedButton;
    sSpeedButton4: TsSpeedButton;
    sLabelFX9: TsLabelFX;
    cek: TsEdit;
    sBitBtn5: TsBitBtn;
    ADOConnection1: TADOConnection;
    ADOTable1: TADOTable;
    DataSource1: TDataSource;
    sComboBox1: TsComboBox;
    procedure sBitBtn1Click(Sender: TObject);
    procedure sBitBtn2Click(Sender: TObject);
    procedure sBitBtn3Click(Sender: TObject);
    procedure sBitBtn4Click(Sender: TObject);
    procedure sSpeedButton1Click(Sender: TObject);
    procedure sSpeedButton2Click(Sender: TObject);
    procedure sSpeedButton3Click(Sender: TObject);
    procedure sSpeedButton4Click(Sender: TObject);
    procedure sBitBtn5Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

procedure TFrame14.sBitBtn1Click(Sender: TObject);
begin
if username.Text='' then
begin
  Showmessage('Username Masih Kosong');
  username.SetFocus;
  end else
if password.Text='' then
begin
  Showmessage('Password Masih Kosong');
  password.SetFocus;
  end else
if level.Text='' then
begin
  Showmessage('Level Masih Kosong');
  level.SetFocus;
  end else
    begin
	  ADOTable1.Open;
	  ADOTable1.Append;
      ADOTable1.FieldByname('username').AsString := username.Text;
      ADOTable1.FieldByName('password').AsString := password.Text;
      ADOTable1.FieldByName('level').AsString := level.Text;
      ADOTable1.Post;
      Showmessage('Data Authentikasi Berhasil Disimpan');
    end;
end;

procedure TFrame14.sBitBtn2Click(Sender: TObject);
begin
if username.Text='' then
begin
  Showmessage('Username Masih Kosong');
  username.SetFocus;
  end else
if password.Text='' then
begin
  Showmessage('Password Masih Kosong');
  password.SetFocus;
  end else
if level.Text='' then
begin
  Showmessage('Level Masih Kosong');
  level.SetFocus;
  end else
    begin
	  ADOTable1.Edit;
      ADOTable1.FieldByname('username').AsString := username.Text;
      ADOTable1.FieldByName('password').AsString := password.Text;
      ADOTable1.FieldByName('level').AsString := level.Text;
      ADOTable1.Post;
      Showmessage('Data Authentikasi Berhasil Diubah');
    end;
end;

procedure TFrame14.sBitBtn3Click(Sender: TObject);
begin
  if messageDlg('Apakah Data '+ADOTable1.FieldByname('username').AsString+' Ingin Dihapus?',mtConfirmation,[Mbyes,Mbno],0)=mryes then
  begin
  ADOTable1.Delete;
  end;
end;

procedure TFrame14.sBitBtn4Click(Sender: TObject);
begin
    username.Text:='';
    password.Text:='';
	  level.Text:='';
end;

procedure TFrame14.sSpeedButton1Click(Sender: TObject);
begin
ADOTable1.First;
end;

procedure TFrame14.sSpeedButton2Click(Sender: TObject);
begin
  ADOTable1.Prior;
  if ADOTable1.BOF then
  ShowMessage('Anda sudah diawal data');
end;

procedure TFrame14.sSpeedButton3Click(Sender: TObject);
begin
  ADOTable1.Next;
  if ADOTable1.EOF then
  ShowMessage('Anda sudah diakhir data');
end;

procedure TFrame14.sSpeedButton4Click(Sender: TObject);
begin
	ADOTable1.Last;
end;

procedure TFrame14.sBitBtn5Click(Sender: TObject);
begin
  If not ADOTable1.locate('level',cek.text,[]) then
  Showmessage('Data Authentikasi Tidak Ditemukan');
end;

end.
