unit laporan_gudang;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, QuickRpt, QRCtrls, DB, ADODB;

type
  TLaporanGudang = class(TQuickRep)
    ADOConnection1: TADOConnection;
    ADOTable1: TADOTable;
    QRSysData2: TQRSysData;
    QRLabel13: TQRLabel;
    DetailBand1: TQRBand;
    kd_gdg: TQRDBText;
    nm_gdg: TQRDBText;
    no_telp: TQRDBText;
    tgl_gdg: TQRDBText;
    wilayah: TQRDBText;
    ket: TQRDBText;
    alamat: TQRDBText;
    QRSysData3: TQRSysData;
    PageHeaderBand1: TQRBand;
    QRLabel12: TQRLabel;
    Lapora: TQRBand;
    QRLabel1: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRSysData4: TQRSysData;
    ColumnHeaderBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRSysData1: TQRSysData;
    QRLabel16: TQRLabel;
  private

  public

  end;

var
  LaporanGudang: TLaporanGudang;

implementation

{$R *.DFM}

end.
