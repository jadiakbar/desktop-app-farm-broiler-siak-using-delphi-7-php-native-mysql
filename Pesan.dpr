program Pesan;

uses
  Forms,
  agen_penjualan in 'agen_penjualan.pas' {FarmBroiler_Login},
  agen in 'agen.pas' {Frame1: TFrame},
  laporan_agen in 'laporan_agen.pas' {LaporanAgen: TQuickRep},
  pegawai in 'pegawai.pas' {Frame2: TFrame},
  laporan_pegawai in 'laporan_pegawai.pas' {LaporanPegawai: TQuickRep},
  barang in 'barang.pas' {Frame3: TFrame},
  laporan_barang in 'laporan_barang.pas' {LaporanBarang: TQuickRep},
  suplier in 'suplier.pas' {Frame4: TFrame},
  laporan_suplier in 'laporan_suplier.pas' {LaporanSuplier: TQuickRep},
  barang_masuk in 'barang_masuk.pas' {Frame5: TFrame},
  laporan_barang_masuk in 'laporan_barang_masuk.pas' {LaporanBarangMasuk: TQuickRep},
  barang_keluar in 'barang_keluar.pas' {Frame6: TFrame},
  laporan_barang_keluar in 'laporan_barang_keluar.pas' {LaporanBarangKeluar: TQuickRep},
  gudang in 'gudang.pas' {Frame7: TFrame},
  laporan_gudang in 'laporan_gudang.pas' {LaporanGudang: TQuickRep},
  stok in 'stok.pas' {Frame8: TFrame},
  stok_barang in 'stok_barang.pas' {Frame9: TFrame},
  data_barang_masuk in 'data_barang_masuk.pas' {Frame10: TFrame},
  data_barang_keluar in 'data_barang_keluar.pas' {Frame12: TFrame},
  login in 'login.pas' {Menu_Kasir},
  coba in 'coba.pas' {Frame11: TFrame},
  Masuk in 'Masuk.pas' {Frame13: TFrame},
  admin in 'admin.pas' {Frame14: TFrame},
  informasi in 'informasi.pas' {Frame15: TFrame},
  help in 'help.pas' {Frame16: TFrame};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TFarmBroiler_Login, FarmBroiler_Login);
  Application.CreateForm(TLaporanAgen, LaporanAgen);
  Application.CreateForm(TLaporanPegawai, LaporanPegawai);
  Application.CreateForm(TLaporanSuplier, LaporanSuplier);
  Application.CreateForm(TLaporanBarang, LaporanBarang);
  Application.CreateForm(TLaporanGudang, LaporanGudang);
  Application.CreateForm(TLaporanBarangKeluar, LaporanBarangKeluar);
  Application.CreateForm(TLaporanBarangMasuk, LaporanBarangMasuk);
  Application.CreateForm(TMenu_Kasir, Menu_Kasir);
  Application.Run;
end.
