unit show_suplier;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, QuickRpt, QRCtrls, DB, ADODB;

type
  TShowSuplier = class(TQuickRep)
    PageHeaderBand1: TQRBand;
    QRLabel12: TQRLabel;
    QRLabel14: TQRLabel;
    TitleBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    DetailBand1: TQRBand;
    kd_agen: TQRDBText;
    nm_agen: TQRDBText;
    nm_ud: TQRDBText;
    alamat: TQRDBText;
    no_telp: TQRDBText;
    tgl_masuk: TQRDBText;
    wilayah: TQRDBText;
    keterangan: TQRDBText;
    QRSysData4: TQRSysData;
    QRLabel1: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    QRLabel13: TQRLabel;
    QRLabel15: TQRLabel;
    QRSysData3: TQRSysData;
    ADOConnection1: TADOConnection;
    ADOTable1: TADOTable;
    ColumnHeaderBand1: TQRBand;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel19: TQRLabel;
  private

  public

  end;

var
  ShowSuplier: TShowSuplier;

implementation

{$R *.DFM}

end.
