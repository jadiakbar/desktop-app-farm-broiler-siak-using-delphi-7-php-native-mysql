unit report_pegawai;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, Grids, DBGrids, DB, ADODB, StdCtrls, Buttons, sBitBtn;

type
  TFrame9 = class(TFrame)
    sBitBtn1: TsBitBtn;
    DataSource1: TDataSource;
    ADOTable1: TADOTable;
    ADOConnection1: TADOConnection;
    DBGrid1: TDBGrid;
    procedure sBitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

uses laporan_pegawai;

{$R *.dfm}

procedure TFrame9.sBitBtn1Click(Sender: TObject);
begin
  LaporanPegawai.Preview;
end;

end.
