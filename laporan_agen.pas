unit laporan_agen;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, QuickRpt, QRCtrls, DB, ADODB;

type
  TLaporanAgen = class(TQuickRep)
    PageHeaderBand1: TQRBand;
    TitleBand1: TQRBand;
    ColumnHeaderBand1: TQRBand;
    DetailBand1: TQRBand;
    ADOConnection1: TADOConnection;
    ADOTable1: TADOTable;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    kd_agen: TQRDBText;
    nm_agen: TQRDBText;
    alamat: TQRDBText;
    no_telp: TQRDBText;
    tgl_masuk: TQRDBText;
    wilayah: TQRDBText;
    keterangan: TQRDBText;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRSysData1: TQRSysData;
    QRLabel12: TQRLabel;
    QRSysData2: TQRSysData;
    QRLabel13: TQRLabel;
    QRSysData4: TQRSysData;
    QRLabel19: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRSysData3: TQRSysData;
  private

  public

  end;

var
  LaporanAgen: TLaporanAgen;

implementation

{$R *.DFM}

end.
