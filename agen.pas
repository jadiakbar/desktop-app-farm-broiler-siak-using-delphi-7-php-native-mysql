unit agen;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, StdCtrls, Buttons, sSpeedButton, Grids, DBGrids, sBitBtn, sEdit,
  sLabel, sGroupBox, DB, ADODB, Mask, sMaskEdit, sCustomComboEdit,
  sTooledit;

type
  TFrame1 = class(TFrame)
    sGroupBox2: TsGroupBox;
    sGroupBox1: TsGroupBox;
    sLabelFX1: TsLabelFX;
    sLabelFX2: TsLabelFX;
    sLabelFX3: TsLabelFX;
    sLabelFX4: TsLabelFX;
    sLabelFX5: TsLabelFX;
    sLabelFX6: TsLabelFX;
    sLabelFX7: TsLabelFX;
    sLabelFX8: TsLabelFX;
    kd_agen: TsEdit;
    nm_agen: TsEdit;
    nm_ud: TsEdit;
    alamat: TsEdit;
    no_telp: TsEdit;
    wilayah: TsEdit;
    keterangan: TsEdit;
    sBitBtn1: TsBitBtn;
    sBitBtn2: TsBitBtn;
    sBitBtn3: TsBitBtn;
    sBitBtn4: TsBitBtn;
    sGroupBox3: TsGroupBox;
    DBGrid1: TDBGrid;
    sSpeedButton1: TsSpeedButton;
    sSpeedButton2: TsSpeedButton;
    sSpeedButton3: TsSpeedButton;
    sSpeedButton4: TsSpeedButton;
    cek: TsEdit;
    sBitBtn5: TsBitBtn;
    sLabelFX9: TsLabelFX;
    ADOConnection1: TADOConnection;
    ADOTable1: TADOTable;
    DataSource1: TDataSource;
    tgl_masuk: TsDateEdit;
    sBitBtn6: TsBitBtn;
    procedure sBitBtn1Click(Sender: TObject);
    procedure sBitBtn2Click(Sender: TObject);
    procedure sBitBtn3Click(Sender: TObject);
    procedure sBitBtn4Click(Sender: TObject);
    procedure sSpeedButton1Click(Sender: TObject);
    procedure sSpeedButton2Click(Sender: TObject);
    procedure sSpeedButton3Click(Sender: TObject);
    procedure sSpeedButton4Click(Sender: TObject);
    procedure sBitBtn5Click(Sender: TObject);
    procedure TBClick(Column: TColumn);
    procedure sBitBtn6Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

uses laporan_barang, laporan_agen;

{$R *.dfm}

procedure TFrame1.sBitBtn1Click(Sender: TObject);
begin
if kd_agen.Text='' then
begin
  Showmessage('Kode Agen Masih Kosong');
  kd_agen.SetFocus;
  end else
if nm_agen.Text='' then
begin
  Showmessage('Nama Agen Masih Kosong');
  nm_agen.SetFocus;
  end else
if nm_ud.Text='' then
begin
  Showmessage('Nama Unit Dagang Masih Kosong');
  nm_ud.SetFocus;
  end else
if alamat.Text='' then
begin
  Showmessage('Alamat Masih Kosong');
  alamat.SetFocus;
  end else
if no_telp.Text='' then
begin
  Showmessage('Nomor Telepon Masih Kosong');
  no_telp.SetFocus;
  end else
if tgl_masuk.Text='' then
begin
  Showmessage('Tanggal Masuk Masih Kosong');
  tgl_masuk.SetFocus;
  end else
if wilayah.Text='' then
begin
  Showmessage('Wilayah Masih Kosong');
  wilayah.SetFocus;
  end else
if keterangan.Text='' then
begin
  Showmessage('Keterangan Masih Kosong');
  keterangan.SetFocus;
  end else
    begin
	  ADOTable1.Open;
	  ADOTable1.Append;
      ADOTable1.FieldByname('kd_agen').AsString := kd_agen.Text;
      ADOTable1.FieldByName('nm_agen').AsString := nm_agen.Text;
      ADOTable1.FieldByName('nm_ud').AsString := nm_ud.Text;
      ADOTable1.FieldByname('alamat').AsString := alamat.Text;
      ADOTable1.FieldByName('no_telp').AsString := no_telp.Text;
      ADOTable1.FieldByName('tgl_masuk').AsString := tgl_masuk.Text;
      ADOTable1.FieldByname('wilayah').AsString := wilayah.Text;
      ADOTable1.FieldByName('keterangan').AsString := keterangan.Text;
      ADOTable1.Post;
      Showmessage('Data Agen Berhasil Disimpan');
    end;
end;

procedure TFrame1.sBitBtn2Click(Sender: TObject);
begin
if kd_agen.Text='' then
begin
  Showmessage('Kode Agen Masih Kosong');
  kd_agen.SetFocus;
  end else
if nm_agen.Text='' then
begin
  Showmessage('Nama Agen Masih Kosong');
  nm_agen.SetFocus;
  end else
if nm_ud.Text='' then
begin
  Showmessage('Nama Unit Dagang Masih Kosong');
  nm_ud.SetFocus;
  end else
if alamat.Text='' then
begin
  Showmessage('Alamat Masih Kosong');
  alamat.SetFocus;
  end else
if no_telp.Text='' then
begin
  Showmessage('Nomor Telepon Masih Kosong');
  no_telp.SetFocus;
  end else
if tgl_masuk.Text='' then
begin
  Showmessage('Tanggal Masuk Masih Kosong');
  tgl_masuk.SetFocus;
  end else
if wilayah.Text='' then
begin
  Showmessage('Wilayah Masih Kosong');
  wilayah.SetFocus;
  end else
if keterangan.Text='' then
begin
  Showmessage('Keterangan Masih Kosong');
  keterangan.SetFocus;
  end else
    begin
	  ADOTable1.Edit;
      ADOTable1.FieldByname('kd_agen').AsString := kd_agen.Text;
      ADOTable1.FieldByName('nm_agen').AsString := nm_agen.Text;
      ADOTable1.FieldByName('nm_ud').AsString := nm_ud.Text;
      ADOTable1.FieldByname('alamat').AsString := alamat.Text;
      ADOTable1.FieldByName('no_telp').AsString := no_telp.Text;
      ADOTable1.FieldByName('tgl_masuk').AsString := tgl_masuk.Text;
      ADOTable1.FieldByname('wilayah').AsString := wilayah.Text;
      ADOTable1.FieldByName('keterangan').AsString := keterangan.Text;
      ADOTable1.Post;
      Showmessage('Data Agen Berhasil Diubah');
    end;
end;

procedure TFrame1.sBitBtn3Click(Sender: TObject);
begin
  if messageDlg('Apakah Kode Agen '+ADOTable1.FieldByname('kd_agen').AsString+' Ingin Dihapus?',mtConfirmation,[Mbyes,Mbno],0)=mryes then
  begin
  ADOTable1.Delete;
  end;
end;

procedure TFrame1.sBitBtn4Click(Sender: TObject);
begin
    kd_agen.Text:='';
    nm_agen.Text:='';
	  nm_ud.Text:='';
    alamat.Text:='';
    no_telp.Text:='';
    tgl_masuk.Text:='';
    wilayah.Text:='';
	keterangan.Text:='';
end;

procedure TFrame1.sSpeedButton1Click(Sender: TObject);
begin
ADOTable1.First;
end;

procedure TFrame1.sSpeedButton2Click(Sender: TObject);
begin
  ADOTable1.Prior;
  if ADOTable1.BOF then
  ShowMessage('Anda sudah diawal data');
end;

procedure TFrame1.sSpeedButton3Click(Sender: TObject);
begin
  ADOTable1.Next;
  if ADOTable1.EOF then
  ShowMessage('Anda sudah diakhir data');
end;

procedure TFrame1.sSpeedButton4Click(Sender: TObject);
begin
	ADOTable1.Last;
end;

procedure TFrame1.sBitBtn5Click(Sender: TObject);
begin
  If not ADOTable1.locate('kd_agen',cek.text,[]) then
  Showmessage('Kode Agen Tidak Ditemukan');
end;

procedure TFrame1.TBClick(Column: TColumn);
begin
  kd_agen.Text := ADOTable1.FieldByname('kd_agen').AsString;
  nm_agen.Text := ADOTable1.FieldByName('nm_agen').AsString;
  nm_ud.Text := ADOTable1.FieldByName('nm_ud').AsString;
  alamat.Text := ADOTable1.FieldByName('alamat').AsString;
  no_telp.Text := ADOTable1.FieldByname('no_telp').AsString;
  tgl_masuk.Text := ADOTable1.FieldByname('tgl_masuk').AsString;
  wilayah.Text := ADOTable1.FieldByname('wilayah').AsString;
  keterangan.Text := ADOTable1.FieldByname('keterangan').AsString;
end;

procedure TFrame1.sBitBtn6Click(Sender: TObject);
begin
  LaporanAgen.Preview;
end;

end.
