unit data_barang_keluar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, sButton, StdCtrls, sEdit, Buttons, sBitBtn, OleCtrls, SHDocVw;

type
  TFrame12 = class(TFrame)
    WebBrowser1: TWebBrowser;
    oke: TsBitBtn;
    sEdit1: TsEdit;
    sBitBtn1: TsBitBtn;
    sEdit2: TsEdit;
    procedure okeClick(Sender: TObject);
    procedure sBitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

procedure TFrame12.okeClick(Sender: TObject);
begin
  WebBrowser1.Navigate(sEdit1.Text);
end;

procedure TFrame12.sBitBtn1Click(Sender: TObject);
begin
  WebBrowser1.Navigate(sEdit2.Text);
end;

end.
