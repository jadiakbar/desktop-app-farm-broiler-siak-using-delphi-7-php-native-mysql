unit barang_masuk;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, Buttons, sSpeedButton, Grids, DBGrids, Mask, sMaskEdit,
  sCustomComboEdit, sTooledit, StdCtrls, sBitBtn, sEdit, sLabel, sGroupBox,
  DB, ADODB, sComboBox, DBCtrls, ExtCtrls;

type
  TFrame5 = class(TFrame)
    sGroupBox1: TsGroupBox;
    sLabelFX1: TsLabelFX;
    sLabelFX2: TsLabelFX;
    sLabelFX3: TsLabelFX;
    sLabelFX4: TsLabelFX;
    sLabelFX6: TsLabelFX;
    sLabelFX7: TsLabelFX;
    sLabelFX8: TsLabelFX;
    brt: TsEdit;
    jmlh: TsEdit;
    sBitBtn1: TsBitBtn;
    tgl_msk: TsDateEdit;
    sGroupBox2: TsGroupBox;
    sGroupBox3: TsGroupBox;
    sSpeedButton1: TsSpeedButton;
    sSpeedButton2: TsSpeedButton;
    sSpeedButton3: TsSpeedButton;
    sSpeedButton4: TsSpeedButton;
    sLabelFX9: TsLabelFX;
    cek: TsEdit;
    ket: TsEdit;
    sLabelFX10: TsLabelFX;
    id_brg_msk: TsEdit;
    ADOConnection1: TADOConnection;
    ADOTable1: TADOTable;
    DataSource1: TDataSource;
    sBitBtn5: TsBitBtn;
    ADOQuery1: TADOQuery;
    Ubah: TsBitBtn;
    Hapus: TsBitBtn;
    Refresh: TsBitBtn;
    DBGrid1: TDBGrid;
    kd_gudang: TsComboBox;
    kd_brg: TsComboBox;
    kd_suplier: TsComboBox;
    kd_peg: TsComboBox;
    sBitBtn2: TsBitBtn;
    procedure sBitBtn1Click(Sender: TObject);
    procedure kd_brgChange(Sender: TObject);
    procedure sBitBtn3Click(Sender: TObject);
    procedure sBitBtn4Click(Sender: TObject);
    procedure TBClick(Column: TColumn);
    procedure sSpeedButton1Click(Sender: TObject);
    procedure sSpeedButton2Click(Sender: TObject);
    procedure sSpeedButton3Click(Sender: TObject);
    procedure sSpeedButton4Click(Sender: TObject);
    procedure sBitBtn5Click(Sender: TObject);
    procedure sComboBox1Change(Sender: TObject);
    procedure UbahClick(Sender: TObject);
    procedure HapusClick(Sender: TObject);
    procedure RefreshClick(Sender: TObject);
    procedure kd_suplierChange(Sender: TObject);
    procedure kd_pegChange(Sender: TObject);
    procedure kd_gudangChange(Sender: TObject);
    procedure sBitBtn2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

uses laporan_barang_masuk;

{$R *.dfm}

procedure TFrame5.sBitBtn1Click(Sender: TObject);
begin
if kd_brg.Text='' then
begin
  Showmessage('Kode Barang Masih Kosong');
  kd_brg.SetFocus;
  end else
if kd_suplier.Text='' then
begin
  Showmessage('Kode Suplier Masih Kosong');
  kd_suplier.SetFocus;
  end else
if kd_gudang.Text='' then
begin
  Showmessage('Kode Gudang Masih Kosong');
  kd_gudang.SetFocus;
  end else
if kd_peg.Text='' then
begin
  Showmessage('Kode Pegawai Masih Kosong');
  kd_peg.SetFocus;
  end else
if jmlh.Text='' then
begin
  Showmessage('Jumlah Masuk Masih Kosong');
  jmlh.SetFocus;
  end else
if brt.Text='' then
begin
  Showmessage('Berat Masih Kosong');
  brt.SetFocus;
  end else
if tgl_msk.Text='' then
begin
  Showmessage('Tanggal Masuk Masih Kosong');
  tgl_msk.SetFocus;
  end else
if ket.Text='' then
begin
  Showmessage('Keterangan Masih Kosong');
  ket.SetFocus;
  end else
    begin
	  ADOTable1.Open;
	  ADOTable1.Append;
      ADOTable1.FieldByName('kd_brg').AsString := kd_brg.Text;
      ADOTable1.FieldByName('kd_suplier').AsString := kd_suplier.Text;
      ADOTable1.FieldByName('kd_gudang').AsString := kd_gudang.Text;
      ADOTable1.FieldByname('kd_peg').AsString := kd_peg.Text;
      ADOTable1.FieldByName('jmlh').AsString := jmlh.Text;
      ADOTable1.FieldByname('brt').AsString := brt.Text;
      ADOTable1.FieldByName('tgl_msk').AsString := tgl_msk.Text;
	    ADOTable1.FieldByName('ket').AsString := ket.Text;
      ADOTable1.Post;
      Showmessage('Data Barang Masuk Berhasil Disimpan');
    end;
end;

procedure TFrame5.kd_brgChange(Sender: TObject);
begin
ADOQuery1.Active:=false;
ADOQuery1.SQL.Clear;
ADOQuery1.SQL.Add('SELECT kd_brg FROM barang');
ADOQuery1.Active:=true;
kd_brg.Items.Clear;
    while not ADOQuery1.Eof do
    begin
        kd_brg.Items.Add(ADOQuery1.FieldByname('kd_brg').AsString);
        ADOQuery1.Next;
    end;
end;

procedure TFrame5.sBitBtn3Click(Sender: TObject);
begin
  if messageDlg('Apakah Identitas Barang Masuk '+ADOTable1.FieldByname('id_brg_msk').AsString+' Ingin Dihapus?',mtConfirmation,[Mbyes,Mbno],0)=mryes then
  begin
  ADOTable1.Delete;
  end;
end;

procedure TFrame5.sBitBtn4Click(Sender: TObject);
begin
    id_brg_msk.Text:='';
    kd_brg.Text:='';
    kd_suplier.Text:='';
	  kd_gudang.Text:='';
    kd_peg.Text:='';
    jmlh.Text:='';
    brt.Text:='';
  	tgl_msk.Text:='';
	  ket.Text:='';
end;

procedure TFrame5.TBClick(Column: TColumn);
begin
  id_brg_msk.Text := ADOTable1.FieldByName('id_brg_msk').AsString;
  kd_brg.Text := ADOTable1.FieldByName('kd_brg').AsString;
  kd_suplier.Text := ADOTable1.FieldByName('kd_suplier').AsString;
  kd_gudang.Text := ADOTable1.FieldByName('kd_gudang').AsString;
  kd_peg.Text := ADOTable1.FieldByName('kd_peg').AsString;
  jmlh.Text := ADOTable1.FieldByname('jmlh').AsString;
  brt.Text := ADOTable1.FieldByname('brt').AsString;
  tgl_msk.Text := ADOTable1.FieldByname('tgl_msk').AsString;
  ket.Text := ADOTable1.FieldByname('ket').AsString;
end;

procedure TFrame5.sSpeedButton1Click(Sender: TObject);
begin
ADOTable1.First;
end;

procedure TFrame5.sSpeedButton2Click(Sender: TObject);
begin
  ADOTable1.Prior;
  if ADOTable1.BOF then
  ShowMessage('Anda sudah diawal data');
end;

procedure TFrame5.sSpeedButton3Click(Sender: TObject);
begin
  ADOTable1.Next;
  if ADOTable1.EOF then
  ShowMessage('Anda sudah diakhir data');
end;

procedure TFrame5.sSpeedButton4Click(Sender: TObject);
begin
	ADOTable1.Last;
end;

procedure TFrame5.sBitBtn5Click(Sender: TObject);
begin
  If not ADOTable1.locate('id_brg_msk',cek.text,[]) then
  Showmessage('Kode Barang Masuk Tidak Ditemukan');
end;

procedure TFrame5.sComboBox1Change(Sender: TObject);
begin
  kd_brg.Items.Clear;
  while not ADOQuery1.Eof do
  begin
    kd_brg.items.add(ADOQuery1.FieldByname('kd_brg').Value);
    ADOQuery1.Next;
  end;
end;

procedure TFrame5.UbahClick(Sender: TObject);
begin
if kd_brg.Text='' then
begin
  Showmessage('Kode Barang Masih Kosong');
  kd_brg.SetFocus;
  end else
if kd_suplier.Text='' then
begin
  Showmessage('Kode Suplier Masih Kosong');
  kd_suplier.SetFocus;
  end else
if kd_gudang.Text='' then
begin
  Showmessage('Kode Gudang Masih Kosong');
  kd_gudang.SetFocus;
  end else
if kd_peg.Text='' then
begin
  Showmessage('Kode Pegawai Masih Kosong');
  kd_peg.SetFocus;
  end else
if jmlh.Text='' then
begin
  Showmessage('Jumlah Masuk Masih Kosong');
  jmlh.SetFocus;
  end else
if brt.Text='' then
begin
  Showmessage('Berat Masih Kosong');
  brt.SetFocus;
  end else
if tgl_msk.Text='' then
begin
  Showmessage('Tanggal Masuk Masih Kosong');
  tgl_msk.SetFocus;
  end else
if ket.Text='' then
begin
  Showmessage('Keterangan Masih Kosong');
  ket.SetFocus;
  end else
    begin
	  ADOTable1.Edit;
      ADOTable1.FieldByName('kd_brg').AsString := kd_brg.Text;
      ADOTable1.FieldByName('kd_suplier').AsString := kd_suplier.Text;
      ADOTable1.FieldByName('kd_gudang').AsString := kd_gudang.Text;
      ADOTable1.FieldByname('kd_peg').AsString := kd_peg.Text;
      ADOTable1.FieldByName('jmlh').AsString := jmlh.Text;
      ADOTable1.FieldByname('brt').AsString := brt.Text;
      ADOTable1.FieldByName('tgl_msk').AsString := tgl_msk.Text;
	    ADOTable1.FieldByName('ket').AsString := ket.Text;
      ADOTable1.Post;
      Showmessage('Data Barang Masuk Berhasil Diubah');
    end;
end;

procedure TFrame5.HapusClick(Sender: TObject);
begin
  if messageDlg('Apakah Identitas Barang Masuk '+ADOTable1.FieldByname('id_brg_msk').AsString+' Ingin Dihapus?',mtConfirmation,[Mbyes,Mbno],0)=mryes then
  begin
  ADOTable1.Delete;
  end;
end;

procedure TFrame5.RefreshClick(Sender: TObject);
begin
    kd_brg.Text:='';
    kd_suplier.Text:='';
	  kd_gudang.Text:='';
    kd_peg.Text:='';
    jmlh.Text:='';
    brt.Text:='';
    tgl_msk.Text:='';
    ket.Text:='';
end;

procedure TFrame5.kd_suplierChange(Sender: TObject);
begin
ADOQuery1.Active:=false;
ADOQuery1.SQL.Clear;
ADOQuery1.SQL.Add('SELECT kd_sup FROM suplier');
ADOQuery1.Active:=true;
kd_suplier.Items.Clear;
    while not ADOQuery1.Eof do
    begin
        kd_suplier.Items.Add(ADOQuery1.FieldByname('kd_sup').AsString);
        ADOQuery1.Next;
    end;
end;

procedure TFrame5.kd_pegChange(Sender: TObject);
begin
ADOQuery1.Active:=false;
ADOQuery1.SQL.Clear;
ADOQuery1.SQL.Add('SELECT kd_peg FROM pegawai');
ADOQuery1.Active:=true;
kd_peg.Items.Clear;
    while not ADOQuery1.Eof do
    begin
        kd_peg.Items.Add(ADOQuery1.FieldByname('kd_peg').AsString);
        ADOQuery1.Next;
    end;
end;
procedure TFrame5.kd_gudangChange(Sender: TObject);
begin
ADOQuery1.Active:=false;
ADOQuery1.SQL.Clear;
ADOQuery1.SQL.Add('SELECT kd_gdg FROM gudang');
ADOQuery1.Active:=true;
kd_suplier.Items.Clear;
    while not ADOQuery1.Eof do
    begin
        kd_gudang.Items.Add(ADOQuery1.FieldByname('kd_gdg').AsString);
        ADOQuery1.Next;
    end;
end;

procedure TFrame5.sBitBtn2Click(Sender: TObject);
begin
  LaporanBarangMasuk.Preview;
end;

end.
