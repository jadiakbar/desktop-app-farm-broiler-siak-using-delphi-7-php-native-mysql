unit data_barang_masuk;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, OleCtrls, SHDocVw, StdCtrls, sEdit, Buttons, sBitBtn;

type
  TFrame10 = class(TFrame)
    WebBrowser1: TWebBrowser;
    show: TsBitBtn;
    laman: TsEdit;
    export_xls: TsBitBtn;
    sEdit1: TsEdit;
    procedure showClick(Sender: TObject);
    procedure export_xlsClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

procedure TFrame10.showClick(Sender: TObject);
begin
  WebBrowser1.Navigate(laman.Text);
end;

procedure TFrame10.export_xlsClick(Sender: TObject);
begin
  WebBrowser1.Navigate(sEdit1.Text);
end;

end.
