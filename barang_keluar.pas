unit barang_keluar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, ADODB, DB, Buttons, sSpeedButton, Grids, DBGrids, StdCtrls,
  Mask, sMaskEdit, sCustomComboEdit, sTooledit, sBitBtn, sEdit, sLabel,
  sGroupBox, sComboBox;

type
  TFrame6 = class(TFrame)
    sGroupBox1: TsGroupBox;
    sLabelFX1: TsLabelFX;
    sLabelFX2: TsLabelFX;
    sLabelFX3: TsLabelFX;
    sLabelFX4: TsLabelFX;
    sLabelFX5: TsLabelFX;
    sLabelFX10: TsLabelFX;
    brt: TsEdit;
    sBitBtn1: TsBitBtn;
    ket: TsEdit;
    no_faktur: TsEdit;
    Ubah: TsBitBtn;
    Hapus: TsBitBtn;
    Refresh: TsBitBtn;
    sGroupBox2: TsGroupBox;
    DBGrid1: TDBGrid;
    sGroupBox3: TsGroupBox;
    sSpeedButton1: TsSpeedButton;
    sSpeedButton2: TsSpeedButton;
    sSpeedButton3: TsSpeedButton;
    sSpeedButton4: TsSpeedButton;
    sLabelFX9: TsLabelFX;
    cek: TsEdit;
    sBitBtn5: TsBitBtn;
    ADOConnection1: TADOConnection;
    ADOTable1: TADOTable;
    DataSource1: TDataSource;
    ADOTable2: TADOTable;
    kd_agen: TsComboBox;
    kd_brg: TsComboBox;
    tgl_psn: TsDateEdit;
    jumlah: TsEdit;
    ADOQuery1: TADOQuery;
    sBitBtn2: TsBitBtn;
    procedure sBitBtn1Click(Sender: TObject);
    procedure UbahClick(Sender: TObject);
    procedure RefreshClick(Sender: TObject);
    procedure HapusClick(Sender: TObject);
    procedure TBClick(Column: TColumn);
    procedure kd_agenChange(Sender: TObject);
    procedure kd_brgChange(Sender: TObject);
    procedure sBitBtn5Click(Sender: TObject);
    procedure sSpeedButton1Click(Sender: TObject);
    procedure sSpeedButton2Click(Sender: TObject);
    procedure sSpeedButton3Click(Sender: TObject);
    procedure sSpeedButton4Click(Sender: TObject);
    procedure sBitBtn2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

uses laporan_barang_keluar;

{$R *.dfm}

procedure TFrame6.sBitBtn1Click(Sender: TObject);
begin
if kd_agen.Text='' then
begin
  Showmessage('Kode Agen Masih Kosong');
  kd_agen.SetFocus;
  end else
if kd_brg.Text='' then
begin
  Showmessage('Kode Barang Masih Kosong');
  kd_brg.SetFocus;
  end else
if tgl_psn.Text='' then
begin
  Showmessage('Tanggal Pesan Masih Kosong');
  tgl_psn.SetFocus;
  end else
if jumlah.Text='' then
begin
  Showmessage('Jumlah Masih Kosong');
  jumlah.SetFocus;
  end else
if brt.Text='' then
begin
  Showmessage('Berat Masih Kosong');
  brt.SetFocus;
  end else
    begin
	  ADOTable1.Open;
	  ADOTable1.Append;
      ADOTable1.FieldByname('kd_agen').AsString := kd_agen.Text;
      ADOTable1.FieldByName('kd_brg').AsString := kd_brg.Text;
      ADOTable1.FieldByName('tgl_psn').AsString := tgl_psn.Text;
      ADOTable1.FieldByname('jumlah').AsString := jumlah.Text;
      ADOTable1.FieldByName('brt').AsString := brt.Text;
      ADOTable1.FieldByName('ket').AsString := ket.Text;
      ADOTable1.Post;
      Showmessage('Data Barang Keluar Berhasil Disimpan');
    end;
end;

procedure TFrame6.UbahClick(Sender: TObject);
begin
if kd_agen.Text='' then
begin
  Showmessage('Kode Agen Masih Kosong');
  kd_agen.SetFocus;
  end else
if kd_brg.Text='' then
begin
  Showmessage('Kode Barang Masih Kosong');
  kd_brg.SetFocus;
  end else
if tgl_psn.Text='' then
begin
  Showmessage('Tanggal Pesan Masih Kosong');
  tgl_psn.SetFocus;
  end else
if jumlah.Text='' then
begin
  Showmessage('Jumlah Masih Kosong');
  jumlah.SetFocus;
  end else
if brt.Text='' then
begin
  Showmessage('Berat Masih Kosong');
  brt.SetFocus;
  end else
    begin
	  ADOTable1.Edit;
      ADOTable1.FieldByname('kd_agen').AsString := kd_agen.Text;
      ADOTable1.FieldByName('kd_brg').AsString := kd_brg.Text;
      ADOTable1.FieldByName('tgl_psn').AsString := tgl_psn.Text;
      ADOTable1.FieldByname('jumlah').AsString := jumlah.Text;
      ADOTable1.FieldByName('brt').AsString := brt.Text;
      ADOTable1.FieldByName('ket').AsString := ket.Text;
      ADOTable1.Post;
      Showmessage('Data Barang Keluar Berhasil Diubah');
    end;
end;

procedure TFrame6.RefreshClick(Sender: TObject);
begin
    kd_agen.Text:='';
    kd_brg.Text:='';
	  tgl_psn.Text:='';
    jumlah.Text:='';
    brt.Text:='';
    ket.Text:='';
end;

procedure TFrame6.HapusClick(Sender: TObject);
begin
  if messageDlg('Apakah Barang Keluar '+ADOTable1.FieldByname('no_faktur').AsString+' Ingin Dihapus?',mtConfirmation,[Mbyes,Mbno],0)=mryes then
  begin
  ADOTable1.Delete;
  end;
end;

procedure TFrame6.TBClick(Column: TColumn);
begin
  no_faktur.Text := ADOTable1.FieldByname('no_faktur').AsString;
  kd_agen.Text := ADOTable1.FieldByName('kd_agen').AsString;
  kd_brg.Text := ADOTable1.FieldByName('kd_brg').AsString;
  tgl_psn.Text := ADOTable1.FieldByName('tgl_psn').AsString;
  jumlah.Text := ADOTable1.FieldByname('jumlah').AsString;
  brt.Text := ADOTable1.FieldByname('brt').AsString;
  ket.Text := ADOTable1.FieldByname('ket').AsString;
end;

procedure TFrame6.kd_agenChange(Sender: TObject);
begin
ADOQuery1.Active:=false;
ADOQuery1.SQL.Clear;
ADOQuery1.SQL.Add('SELECT kd_agen FROM agen');
ADOQuery1.Active:=true;
kd_agen.Items.Clear;
    while not ADOQuery1.Eof do
    begin
        kd_agen.Items.Add(ADOQuery1.FieldByname('kd_agen').Value);
        ADOQuery1.Next;
    end;
end;

procedure TFrame6.kd_brgChange(Sender: TObject);
begin
ADOQuery1.Active:=false;
ADOQuery1.SQL.Clear;
ADOQuery1.SQL.Add('SELECT kd_brg FROM barang');
ADOQuery1.Active:=true;
kd_brg.Items.Clear;
    while not ADOQuery1.Eof do
    begin
        kd_brg.Items.Add(ADOQuery1.FieldByname('kd_brg').Value);
        ADOQuery1.Next;
    end;
end;

procedure TFrame6.sBitBtn5Click(Sender: TObject);
begin
  If not ADOTable1.locate('no_faktur',cek.text,[]) then
  Showmessage('Nomor Faktur Tidak Ditemukan');
end;

procedure TFrame6.sSpeedButton1Click(Sender: TObject);
begin
ADOTable1.First;
end;

procedure TFrame6.sSpeedButton2Click(Sender: TObject);
begin
  ADOTable1.Prior;
  if ADOTable1.BOF then
  ShowMessage('Anda sudah diawal data');
end;

procedure TFrame6.sSpeedButton3Click(Sender: TObject);
begin
  ADOTable1.Next;
  if ADOTable1.EOF then
  ShowMessage('Anda sudah diakhir data');
end;

procedure TFrame6.sSpeedButton4Click(Sender: TObject);
begin
	ADOTable1.Last;
end;

procedure TFrame6.sBitBtn2Click(Sender: TObject);
begin
  LaporanBarangKeluar.Preview;
end;

end.
