unit laporan_barang_masuk;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, QuickRpt, QRCtrls, DB, ADODB;

type
  TLaporanBarangMasuk = class(TQuickRep)
    PageHeaderBand1: TQRBand;
    TitleBand1: TQRBand;
    ColumnHeaderBand1: TQRBand;
    DetailBand1: TQRBand;
    ADOConnection1: TADOConnection;
    ADOTable1: TADOTable;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    tgl_entry: TQRDBText;
    kd_brg: TQRDBText;
    kd_suplier: TQRDBText;
    kd_gudang: TQRDBText;
    wilayah: TQRDBText;
    ket: TQRDBText;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRSysData1: TQRSysData;
    QRLabel12: TQRLabel;
    QRSysData2: TQRSysData;
    QRLabel13: TQRLabel;
    QRSysData4: TQRSysData;
    QRLabel19: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRSysData3: TQRSysData;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel6: TQRLabel;
    kd_peg: TQRDBText;
    brt: TQRDBText;
    jmlh: TQRDBText;
    ADOTable2: TADOTable;
  private

  public

  end;

var
  LaporanBarangMasuk: TLaporanBarangMasuk;

implementation

{$R *.DFM}

end.
