<?php require_once('Connections/si_farm_broiler.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_si_farm_broiler, $si_farm_broiler);
$query_brg_klr = "SELECT barang_keluar.no_faktur, barang_keluar.tgl_psn, barang_keluar.kd_agen, barang_keluar.kd_brg, barang.nm_brg, barang_keluar.brt, barang.satuan, (SUM(barang_keluar.brt)*barang.hrg_jual) AS Total, (SUM(barang_keluar.brt)*barang.hrg_satuan) AS Modal, barang_keluar.ket, barang_keluar.tgl_transaksi FROM barang LEFT JOIN barang_keluar ON barang_keluar.kd_brg=barang.kd_brg GROUP BY barang_keluar.no_faktur";
$brg_klr = mysql_query($query_brg_klr, $si_farm_broiler) or die(mysql_error());
$row_brg_klr = mysql_fetch_assoc($brg_klr);
$totalRows_brg_klr = mysql_num_rows($brg_klr);

$i = 1;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Data Barang Keluar</title>
<style type="text/css">

.fontku {
	font-size: 10px;
}
</style>
</head>

<body>
<table width="950" border="1" cellpadding="0" cellspacing="0">
  <tr bgcolor="#33CCFF">
    <td><div align="center"><strong><span class="fontku">NO</span></strong></div></td>
    <td><div align="center"><strong><span class="fontku">NO. FAKTUR</span></strong></div></td>
    <td><div align="center"><strong><span class="fontku">KODE AGEN</span></strong></div></td>
    <td><div align="center"><strong><span class="fontku">KODE BARANG</span></strong></div></td>
    <td><div align="center"><strong><span class="fontku">NAMA BARANG</span></strong></div></td>
    <td class="fontku"><div align="center"><strong>BERAT</strong></div></td>
    <td class="fontku"><div align="center"><strong>SATUAN</strong></div></td>
    <td><div align="center"><strong><span class="fontku">TGL PESAN</span></strong></div></td>
    <td><div align="center"><strong><span class="fontku">TOTAL</span></strong></div></td>
    <td><div align="center"><strong><span class="fontku">MODAL</span></strong></div></td>
    <td><div align="center"><strong><span class="fontku">KEUNTUNGAN</span></strong></div></td>
    <td><div align="center"><strong><span class="fontku">TGL TRANSAKSI</span></strong></div></td>
    <td><div align="center"><strong><span class="fontku">KETERANGAN</span></strong></div></td>
  </tr>
  <?php do { ?>
    <tr>
      <td><div align="center"><span class="fontku"><?php echo $i; ?></span></div></td>
      <td><div align="center"><span class="fontku"><?php echo $row_brg_klr['no_faktur']; ?></span></div></td>
      <td><div align="center"><span class="fontku"><?php echo $row_brg_klr['kd_agen']; ?></span></div></td>
      <td><div align="center"><span class="fontku"><?php echo $row_brg_klr['kd_brg']; ?></span></div></td>
      <td><div align="center"><span class="fontku"><?php echo $row_brg_klr['nm_brg']; ?></span></div></td>
      <td class="fontku"><div align="center"><?php echo $row_brg_klr['brt']; ?></div></td>
      <td class="fontku"><div align="center"><?php echo $row_brg_klr['satuan']; ?></div></td>
      <td><div align="center"><span class="fontku"><?php echo $row_brg_klr['tgl_psn']; ?></span></div></td>
      <td><div align="center"><span class="fontku">Rp.<?php echo $row_brg_klr['Total']; ?>.-</span></div></td>
      <td><div align="center"><span class="fontku">Rp.<?php echo $row_brg_klr['Modal']; ?>.-</span></div></td>
      <td><div align="center"><span class="fontku">Rp.<?php echo $row_brg_klr['Total']-$row_brg_klr['Modal']; ?>.-</span></div></td>
      <td><div align="center"><span class="fontku"><?php echo $row_brg_klr['tgl_transaksi']; ?></span></div></td>
      <td><div align="center"><span class="fontku"><?php echo $row_brg_klr['ket']; ?></span></div></td>
    </tr> <?php $i++ ?>
    <?php } while ($row_brg_klr = mysql_fetch_assoc($brg_klr)); ?>
</table>
</body>
</html>
<?php
mysql_free_result($brg_klr);
?>
