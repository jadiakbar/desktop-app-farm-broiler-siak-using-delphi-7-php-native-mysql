<?php require_once('Connections/si_farm_broiler.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_si_farm_broiler, $si_farm_broiler);
$query_brg_msk = "SELECT barang_masuk.tgl_entry, barang_masuk.kd_brg, barang.nm_brg , SUM(barang_masuk.brt) AS Jumlah, barang.satuan, (SUM(barang_masuk.brt)*barang.hrg_satuan) AS Total, barang_masuk.kd_suplier, barang_masuk.kd_gudang, barang_masuk.kd_peg, barang_masuk.brt, barang_masuk.tgl_msk, barang_masuk.ket FROM barang LEFT JOIN  barang_masuk ON barang_masuk.kd_brg=barang.kd_brg GROUP BY barang_masuk.id_brg_msk ORDER BY barang_masuk.tgl_entry ASC";
$brg_msk = mysql_query($query_brg_msk, $si_farm_broiler) or die(mysql_error());
$row_brg_msk = mysql_fetch_assoc($brg_msk);
$totalRows_brg_msk = mysql_num_rows($brg_msk);
 
$i = 1;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Data Barang Masuk</title>
<style type="text/css">

.pont {
	font-size: 10px;
}
</style>
</head>

<body>
<table width="950" border="1" cellpadding="0" cellspacing="0">
  <tr bgcolor="#33CCFF">
    <td class="pont"><div align="center"><strong>No</strong></div></td>
    <td><div align="center" class="pont"><strong>TGL ENTRI</strong></div></td>
    <td><div align="center"><strong><span class="pont">KODE BARANG</span></strong></div></td>
    <td><div align="center"><strong><span class="pont">NAMA BARANG</span></strong></div></td>
    <td><div align="center" class="pont"><strong>JUMLAH</strong></div></td>
    <td><div align="center"><strong><span class="pont">SATUAN</span></strong></div></td>
    <td><div align="center"><strong><span class="pont">TOTAL MODAL</span></strong></div></td>
    <td><div align="center"><strong><span class="pont">KODE SUPLIER</span></strong></div></td>
    <td><div align="center"><strong><span class="pont">KODE GUDANG</span></strong></div></td>
    <td><div align="center"><strong><span class="pont">KODE PEGAWAI</span></strong></div></td>
    <td><div align="center"><strong><span class="pont">BERAT</span></strong></div></td>
    <td><div align="center"><strong><span class="pont">TGL MASUK</span></strong></div></td>
    <td><div align="center" class="pont"><strong>KETERANGAN</strong></div></td>
  </tr>
  <?php do { ?>
    <tr>
      <td class="pont"><div align="center"><?php echo $i; ?></div></td>
      <td><div align="center"><span class="pont"><?php echo $row_brg_msk['tgl_entry']; ?></span></div></td>
      <td><div align="center"><span class="pont"><?php echo $row_brg_msk['kd_brg']; ?></span></div></td>
      <td><div align="center"><span class="pont"><?php echo $row_brg_msk['nm_brg']; ?></span></div></td>
      <td><div align="center"><span class="pont"><?php echo $row_brg_msk['Jumlah']; ?></span></div></td>
      <td><div align="center"><span class="pont"><?php echo $row_brg_msk['satuan']; ?></span></div></td>
      <td><div align="center"><span class="pont">Rp. <?php echo $row_brg_msk['Total']; ?>.-</span></div></td>
      <td><div align="center"><span class="pont"><?php echo $row_brg_msk['kd_suplier']; ?></span></div></td>
      <td><div align="center"><span class="pont"><?php echo $row_brg_msk['kd_gudang']; ?></span></div></td>
      <td><div align="center"><span class="pont"><?php echo $row_brg_msk['kd_peg']; ?></span></div></td>
      <td><div align="center"><span class="pont"><?php echo $row_brg_msk['brt']; ?></span></div></td>
      <td><div align="center"><span class="pont"><?php echo $row_brg_msk['tgl_msk']; ?></span></div></td>
      <td><div align="center"><span class="pont"><?php echo $row_brg_msk['ket']; ?></span></div></td>
    </tr> <?php $i++ ?>
    <?php } while ($row_brg_msk = mysql_fetch_assoc($brg_msk)); ?>
</table>
<p>&nbsp;</p>
</body>
</html>
<?php
mysql_free_result($brg_msk);
?>
