<?php require_once('Connections/si_farm_broiler.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_si_farm_broiler, $si_farm_broiler);
$query_stok = "SELECT SUM(barang_masuk.jmlh) AS Jumlah, barang.nm_brg FROM barang LEFT JOIN barang_masuk ON barang_masuk.kd_brg=barang.kd_brg GROUP BY barang.kd_brg";
$stok = mysql_query($query_stok, $si_farm_broiler) or die(mysql_error());
$row_stok = mysql_fetch_assoc($stok);
$totalRows_stok = mysql_num_rows($stok);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Grafik Barang Masuk</title>
</head>

<body>
<script type="text/javascript" src="JS/jquery-1.4.js"></script>
<script type="text/javascript" src="JS/jquery.fusioncharts.js"></script>
<div align="center">
  <table width="530" border="1" cellpadding="0" cellspacing="0" id="Barang_Masuk" table>
    <tr bgcolor="#33CCFF">
      <td><div align="center"><strong>Nama Barang</strong></div></td>
      <td><div align="center"><strong>Jumlah Barang Masuk</strong></div></td>
    </tr>
    <?php do { ?>
      <tr>
        <td><div align="center"><?php echo $row_stok['nm_brg']; ?></div></td>
        <td><div align="center"><?php echo $row_stok['Jumlah']; ?></div></td>
      </tr>
      <?php } while ($row_stok = mysql_fetch_assoc($stok)); ?>
  </table>
</div>
<p>&nbsp;</p>
</body>
</html>
<?php
mysql_free_result($stok);
?>
<!--LOAD HTML KE JQUERY FUSION CHART BERDASARKAN ID TABLE-->
<script type="text/javascript">
    $('#Barang_Masuk').convertToFusionCharts({
        swfPath: "Charts/",
        type: "MSColumn3D",
        data: "#Barang_Masuk",
        dataFormat: "HTMLTable"
    });
</script>
<!-- C0d393n Creative -->