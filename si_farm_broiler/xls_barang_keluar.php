<?php require_once('Connections/si_farm_broiler.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_si_farm_broiler, $si_farm_broiler);
$query_brg_klr = "SELECT barang_keluar.no_faktur, barang_keluar.tgl_psn, barang_keluar.kd_agen, barang_keluar.kd_brg, barang.nm_brg, barang_keluar.brt, barang.satuan, (SUM(barang_keluar.brt)*barang.hrg_jual) AS Total, (SUM(barang_keluar.brt)*barang.hrg_satuan) AS Modal, barang_keluar.ket, barang_keluar.tgl_transaksi FROM barang LEFT JOIN barang_keluar ON barang_keluar.kd_brg=barang.kd_brg GROUP BY barang_keluar.no_faktur";
$brg_klr = mysql_query($query_brg_klr, $si_farm_broiler) or die(mysql_error());
$row_brg_klr = mysql_fetch_assoc($brg_klr);
$totalRows_brg_klr = mysql_num_rows($brg_klr);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<style type="text/css">
.warna_judul {
	color: #3CC;
	font-size: 18px;
}
.warna_judul strong {
	font-size: 24px;
}
.fontku {	font-size: 10px;
}
</style>
</head>

<body>
<p>
  <?php
header("Content-Type: application/vnd.ms-excel");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("content-disposition: attachment;filename=Barang_Keluar.xls");
?>
</p>
<p align="center" class="warna_judul"><strong>CV. FARM BROILER</strong></p>
<p align="center" class="warna_judul">Jl. Padat Karya, No.2 - Kabupaten Siak</p>
<table width="500" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <th width="24" scope="col">&nbsp;</th>
    <th width="308" scope="col"><div align="left">NAMA DATA</div></th>
    <th width="168" scope="col"><div align="left">: BARANG KELUAR</div></th>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><div align="left"><strong>JUMLAH DATA</strong></div></td>
    <td><div align="left"><strong>: <?php echo $totalRows_brg_klr ?> </strong></div></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><div align="left"></div></td>
    <td><div align="left"></div></td>
  </tr>
</table>
<table width="950" border="1" cellpadding="0" cellspacing="0">
  <tr bgcolor="#33CCFF">
    <td><div align="center"><strong>NO</strong></div></td>
    <td><div align="center"><strong>NO. FAKTUR</strong></div></td>
    <td><div align="center"><strong>KODE AGEN</strong></div></td>
    <td><div align="center"><strong>KODE BARANG</strong></div></td>
    <td><div align="center"><strong>NAMA BARANG</strong></div></td>
    <td><div align="center"><strong>TGL PESAN</strong></div></td>
    <td><div align="center"><strong>TOTAL</strong></div></td>
    <td><div align="center"><strong>MODAL</strong></div></td>
    <td><div align="center"><strong>KEUNTUNGAN</strong></div></td>
    <td><div align="center"><strong>TGL TRANSAKSI</strong></div></td>
    <td><div align="center"><strong>KETERANGAN</strong></div></td>
  </tr>
  <?php do { ?>
  <tr>
    <td><div align="center"><?php echo $i; ?></div></td>
    <td><div align="center"><?php echo $row_brg_klr['no_faktur']; ?></div></td>
    <td><div align="center"><?php echo $row_brg_klr['kd_agen']; ?></div></td>
    <td><div align="center"><?php echo $row_brg_klr['kd_brg']; ?></div></td>
    <td><div align="center"><?php echo $row_brg_klr['nm_brg']; ?></div></td>
    <td><div align="center"><?php echo $row_brg_klr['tgl_psn']; ?></div></td>
    <td><div align="center"><?php echo $row_brg_klr['Total']; ?></div></td>
    <td><div align="center"><?php echo $row_brg_klr['Modal']; ?></div></td>
    <td><div align="center"><?php echo $row_brg_klr['Total']-$row_brg_klr['Modal']; ?></div></td>
    <td><div align="center"><?php echo $row_brg_klr['tgl_transaksi']; ?></div></td>
    <td><div align="center"><?php echo $row_brg_klr['ket']; ?></div></td>
  </tr>
  <?php $i++ ?>
  <?php } while ($row_brg_klr = mysql_fetch_assoc($brg_klr)); ?>
</table>
<p>&nbsp;</p>
</body>
</html>
<?php
mysql_free_result($brg_klr);
?>
