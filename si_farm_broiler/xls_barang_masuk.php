<?php require_once('Connections/si_farm_broiler.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

mysql_select_db($database_si_farm_broiler, $si_farm_broiler);
$query_msk = "SELECT barang_masuk.tgl_entry, barang_masuk.kd_brg, barang.nm_brg , SUM(barang_masuk.brt) AS Jumlah, barang.satuan, (SUM(barang_masuk.brt)*barang.hrg_satuan) AS Total, barang_masuk.kd_suplier, barang_masuk.kd_gudang, barang_masuk.kd_peg, barang_masuk.brt, barang_masuk.tgl_msk, barang_masuk.ket FROM barang LEFT JOIN  barang_masuk ON barang_masuk.kd_brg=barang.kd_brg GROUP BY barang_masuk.id_brg_msk";
$msk = mysql_query($query_msk, $si_farm_broiler) or die(mysql_error());
$row_msk = mysql_fetch_assoc($msk);
$totalRows_msk = mysql_num_rows($msk);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<style type="text/css">
.warna_judul {
	color: #3CC;
	font-size: 18px;
}
.warna_judul strong {
	font-size: 24px;
}
</style>
</head>

<body>
<p>
  <?php
header("Content-Type: application/vnd.ms-excel");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("content-disposition: attachment;filename=Barang_Masuk.xls");
?>
</p>
<p align="center" class="warna_judul"><strong>CV. FARM BROILER</strong></p>
<p align="center" class="warna_judul">Jl. Padat Karya, No.2 - Kabupaten Siak</p>
<table width="500" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <th width="24" scope="col">&nbsp;</th>
    <th width="308" scope="col"><div align="left">NAMA DATA</div></th>
    <th width="168" scope="col"><div align="left">: BARANG MASUK</div></th>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><div align="left"><strong>JUMLAH DATA</strong></div></td>
    <td><div align="left"><strong>: <?php echo $totalRows_msk ?> </strong></div></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><div align="left"></div></td>
    <td><div align="left"></div></td>
  </tr>
</table>
<table border="1" cellpadding="0" cellspacing="0">
  <tr bgcolor="#33CCFF">
    <td><div align="center"><strong>No</strong></div></td>
    <td><div align="center"><strong>TGL ENTRI</strong></div></td>
    <td><div align="center"><strong>KODE BARANG</strong></div></td>
    <td><div align="center"><strong>NAMA BARANG</strong></div></td>
    <td><div align="center"><strong>JUMLAH</strong></div></td>
    <td><div align="center"><strong>SATUAN</strong></div></td>
    <td><div align="center"><strong>TOTAL MODAL</strong></div></td>
    <td><div align="center"><strong>KODE SUPLIER</strong></div></td>
    <td><div align="center"><strong>KODE GUDANG</strong></div></td>
    <td><div align="center"><strong>KODE PEGAWAI</strong></div></td>
    <td><div align="center"><strong>BERAT</strong></div></td>
    <td><div align="center"><strong>TGL MASUK</strong></div></td>
    <td><div align="center"><strong>KETERANGAN</strong></div></td>
  </tr>
  <?php do { ?>
    <tr>
      <td><div align="center">
        <?php $i; ?>
      </div></td>
      <td><div align="center"><?php echo $row_msk['tgl_entry']; ?></div></td>
      <td><div align="center"><?php echo $row_msk['kd_brg']; ?></div></td>
      <td><div align="center"><?php echo $row_msk['nm_brg']; ?></div></td>
      <td><div align="center"><?php echo $row_msk['Jumlah']; ?></div></td>
      <td><div align="center"><?php echo $row_msk['satuan']; ?></div></td>
      <td><div align="center"><?php echo $row_msk['Total']; ?></div></td>
      <td><div align="center"><?php echo $row_msk['kd_suplier']; ?></div></td>
      <td><div align="center"><?php echo $row_msk['kd_gudang']; ?></div></td>
      <td><div align="center"><?php echo $row_msk['kd_peg']; ?></div></td>
      <td><div align="center"><?php echo $row_msk['brt']; ?></div></td>
      <td><div align="center"><?php echo $row_msk['tgl_msk']; ?></div></td>
      <td><div align="center"><?php echo $row_msk['ket']; ?></div></td>
    </tr> <?php $i++ ?>
    <?php } while ($row_msk = mysql_fetch_assoc($msk)); ?>
</table>
</body>
</html>
<?php
mysql_free_result($msk);
?>
