unit login;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, sBitBtn, sEdit, sLabel, DB, ADODB,
  sSkinProvider, sSkinManager, Menus, Masuk, data_barang_keluar,
  data_barang_masuk, stok_barang, gudang, barang_keluar, barang_masuk,
  suplier, barang, pegawai, agen, admin, informasi, help;

type
  TMenu_Kasir = class(TForm)
    sLabelFX1: TsLabelFX;
    sLabelFX2: TsLabelFX;
    sLabelFX3: TsLabelFX;
    sLabelFX4: TsLabelFX;
    sLabelFX5: TsLabelFX;
    sLabelFX6: TsLabelFX;
    sLabelFX7: TsLabelFX;
    MainMenu1: TMainMenu;
    A1: TMenuItem;
    A2: TMenuItem;
    P1: TMenuItem;
    S2: TMenuItem;
    G1: TMenuItem;
    E1: TMenuItem;
    I1: TMenuItem;
    B1: TMenuItem;
    S1: TMenuItem;
    T1: TMenuItem;
    B2: TMenuItem;
    B3: TMenuItem;
    V1: TMenuItem;
    B4: TMenuItem;
    B5: TMenuItem;
    H1: TMenuItem;
    H2: TMenuItem;
    I2: TMenuItem;
    T2: TMenuItem;
    sSkinManager1: TsSkinManager;
    sSkinProvider1: TsSkinProvider;
    Frame121: TFrame12;
    Frame101: TFrame10;
    Frame91: TFrame9;
    Frame71: TFrame7;
    Frame61: TFrame6;
    Frame51: TFrame5;
    Frame41: TFrame4;
    Frame31: TFrame3;
    Frame21: TFrame2;
    Frame11: TFrame1;
    A3: TMenuItem;
    Frame141: TFrame14;
    Frame151: TFrame15;
    Frame161: TFrame16;
    procedure A2Click(Sender: TObject);
    procedure P1Click(Sender: TObject);
    procedure S2Click(Sender: TObject);
    procedure G1Click(Sender: TObject);
    procedure B1Click(Sender: TObject);
    procedure S1Click(Sender: TObject);
    procedure B2Click(Sender: TObject);
    procedure B3Click(Sender: TObject);
    procedure B4Click(Sender: TObject);
    procedure B5Click(Sender: TObject);
    procedure E1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure A3Click(Sender: TObject);
    procedure I2Click(Sender: TObject);
    procedure T2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Menu_Kasir: TMenu_Kasir;

implementation

uses agen_penjualan;

{$R *.dfm}

procedure TMenu_Kasir.A2Click(Sender: TObject);
begin
  Frame11.Visible:=true;
  Frame21.Visible:=false;
  Frame31.Visible:=false;
  Frame41.Visible:=false;
  Frame51.Visible:=false;
  Frame61.Visible:=false;
  Frame71.Visible:=false;
  Frame91.Visible:=false;
  Frame101.Visible:=false;
  Frame121.Visible:=false;
  Frame141.Visible:=false;
  Frame151.Visible:=false;
  Frame161.Visible:=false;
end;

procedure TMenu_Kasir.P1Click(Sender: TObject);
begin
  Frame11.Visible:=false;
  Frame21.Visible:=true;
  Frame31.Visible:=false;
  Frame41.Visible:=false;
  Frame51.Visible:=false;
  Frame61.Visible:=false;
  Frame71.Visible:=false;
  Frame91.Visible:=false;
  Frame101.Visible:=false;
  Frame121.Visible:=false;
  Frame141.Visible:=false;
  Frame151.Visible:=false;
  Frame161.Visible:=false;
end;

procedure TMenu_Kasir.S2Click(Sender: TObject);
begin
  Frame11.Visible:=false;
  Frame21.Visible:=false;
  Frame31.Visible:=false;
  Frame41.Visible:=true;
  Frame51.Visible:=false;
  Frame61.Visible:=false;
  Frame71.Visible:=false;
  Frame91.Visible:=false;
  Frame101.Visible:=false;
  Frame121.Visible:=false;
  Frame141.Visible:=false;
  Frame151.Visible:=false;
  Frame161.Visible:=false;
end;

procedure TMenu_Kasir.G1Click(Sender: TObject);
begin
  Frame11.Visible:=false;
  Frame21.Visible:=false;
  Frame31.Visible:=false;
  Frame41.Visible:=false;
  Frame51.Visible:=false;
  Frame61.Visible:=false;
  Frame71.Visible:=true;
  Frame91.Visible:=false;
  Frame101.Visible:=false;
  Frame121.Visible:=false;
  Frame141.Visible:=false;
  Frame151.Visible:=false;
  Frame161.Visible:=false;
end;

procedure TMenu_Kasir.B1Click(Sender: TObject);
begin
  Frame11.Visible:=false;
  Frame21.Visible:=false;
  Frame31.Visible:=true;
  Frame41.Visible:=false;
  Frame51.Visible:=false;
  Frame61.Visible:=false;
  Frame71.Visible:=false;
  Frame91.Visible:=false;
  Frame101.Visible:=false;
  Frame121.Visible:=false;
  Frame141.Visible:=false;
  Frame151.Visible:=false;
  Frame161.Visible:=false;
end;

procedure TMenu_Kasir.S1Click(Sender: TObject);
begin
  Frame11.Visible:=false;
  Frame21.Visible:=false;
  Frame31.Visible:=false;
  Frame41.Visible:=false;
  Frame51.Visible:=false;
  Frame61.Visible:=false;
  Frame71.Visible:=false;
  Frame91.Visible:=true;
  Frame101.Visible:=false;
  Frame121.Visible:=false;
  Frame141.Visible:=false;
  Frame151.Visible:=false;
  Frame161.Visible:=false;
end;

procedure TMenu_Kasir.B2Click(Sender: TObject);
begin
  Frame11.Visible:=false;
  Frame21.Visible:=false;
  Frame31.Visible:=false;
  Frame41.Visible:=false;
  Frame51.Visible:=true;
  Frame61.Visible:=false;
  Frame71.Visible:=false;
  Frame91.Visible:=false;
  Frame101.Visible:=false;
  Frame121.Visible:=false;
  Frame141.Visible:=false;
  Frame151.Visible:=false;
  Frame161.Visible:=false;
end;

procedure TMenu_Kasir.B3Click(Sender: TObject);
begin
  Frame11.Visible:=false;
  Frame21.Visible:=false;
  Frame31.Visible:=false;
  Frame41.Visible:=false;
  Frame51.Visible:=false;
  Frame61.Visible:=true;
  Frame71.Visible:=false;
  Frame91.Visible:=false;
  Frame101.Visible:=false;
  Frame121.Visible:=false;
  Frame141.Visible:=false;
  Frame151.Visible:=false;
  Frame161.Visible:=false;
end;

procedure TMenu_Kasir.B4Click(Sender: TObject);
begin
  Frame11.Visible:=false;
  Frame21.Visible:=false;
  Frame31.Visible:=false;
  Frame41.Visible:=false;
  Frame51.Visible:=false;
  Frame61.Visible:=false;
  Frame71.Visible:=false;
  Frame91.Visible:=false;
  Frame101.Visible:=true;
  Frame121.Visible:=false;
  Frame141.Visible:=false;
  Frame151.Visible:=false;
  Frame161.Visible:=false;
end;

procedure TMenu_Kasir.B5Click(Sender: TObject);
begin
  Frame11.Visible:=false;
  Frame21.Visible:=false;
  Frame31.Visible:=false;
  Frame41.Visible:=false;
  Frame51.Visible:=false;
  Frame61.Visible:=false;
  Frame71.Visible:=false;
  Frame91.Visible:=false;
  Frame101.Visible:=false;
  Frame121.Visible:=true;
  Frame141.Visible:=false;
  Frame151.Visible:=false;
  Frame161.Visible:=false;
end;

procedure TMenu_Kasir.E1Click(Sender: TObject);
begin
  FarmBroiler_Login.Close;
  Menu_Kasir.Close;
end;

procedure TMenu_Kasir.FormCreate(Sender: TObject);
begin
  Frame11.Visible:=true;
  Frame21.Visible:=false;
  Frame31.Visible:=false;
  Frame41.Visible:=false;
  Frame51.Visible:=false;
  Frame61.Visible:=false;
  Frame71.Visible:=false;
  Frame91.Visible:=false;
  Frame101.Visible:=false;
  Frame121.Visible:=false;
  Frame141.Visible:=false;
  Frame151.Visible:=false;
  Frame161.Visible:=false;
end;

procedure TMenu_Kasir.A3Click(Sender: TObject);
begin
  Frame11.Visible:=false;
  Frame21.Visible:=false;
  Frame31.Visible:=false;
  Frame41.Visible:=false;
  Frame51.Visible:=false;
  Frame61.Visible:=false;
  Frame71.Visible:=false;
  Frame91.Visible:=false;
  Frame101.Visible:=false;
  Frame121.Visible:=false;
  Frame141.Visible:=true;
  Frame151.Visible:=false;
  Frame161.Visible:=false;
end;

procedure TMenu_Kasir.I2Click(Sender: TObject);
begin
  Frame11.Visible:=false;
  Frame21.Visible:=false;
  Frame31.Visible:=false;
  Frame41.Visible:=false;
  Frame51.Visible:=false;
  Frame61.Visible:=false;
  Frame71.Visible:=false;
  Frame91.Visible:=false;
  Frame101.Visible:=false;
  Frame121.Visible:=false;
  Frame141.Visible:=false;
  Frame151.Visible:=true;
  Frame161.Visible:=false;
end;

procedure TMenu_Kasir.T2Click(Sender: TObject);
begin
  Frame11.Visible:=false;
  Frame21.Visible:=false;
  Frame31.Visible:=false;
  Frame41.Visible:=false;
  Frame51.Visible:=false;
  Frame61.Visible:=false;
  Frame71.Visible:=false;
  Frame91.Visible:=false;
  Frame101.Visible:=false;
  Frame121.Visible:=false;
  Frame141.Visible:=false;
  Frame151.Visible:=false;
  Frame161.Visible:=true;
end;

end.
