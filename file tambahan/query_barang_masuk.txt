SELECT barang_masuk.tgl_entry, barang_masuk.kd_brg, barang.nm_brg , SUM(barang_masuk.brt) AS Jumlah, barang.satuan, (SUM(barang_masuk.brt)*barang.hrg_satuan) AS Total, barang_masuk.kd_suplier, barang_masuk.kd_gudang, barang_masuk.kd_peg, barang_masuk.brt, barang_masuk.tgl_msk, barang_masuk.ket
FROM barang LEFT JOIN  barang_masuk ON barang_masuk.kd_brg=barang.kd_brg
GROUP BY barang_masuk.id_brg_msk