if kd_agen.Text='' then
begin
  Showmessage('Kode Agen Masih Kosong');
  kd_agen.SetFocus;
  end else
if kd_brg.Text='' then
begin
  Showmessage('Kode Barang Masih Kosong');
  kd_brg.SetFocus;
  end else
if tgl_psn.Text='' then
begin
  Showmessage('Tanggal Pesan Masih Kosong');
  tgl_psn.SetFocus;
  end else
if jumlah.Text='' then
begin
  Showmessage('Jumlah Masih Kosong');
  jumlah.SetFocus;
  end else
if brt.Text='' then
begin
  Showmessage('Berat Masih Kosong');
  brt.SetFocus;
  end else
    begin
	  ADOTable1.Open;
	  ADOTable1.Append;
      ADOTable1.FieldByname('kd_agen').AsString := kd_agen.Text;
      ADOTable1.FieldByName('kd_brg').AsString := kd_brg.Text;
      ADOTable1.FieldByName('tgl_psn').AsString := tgl_psn.Text;
      ADOTable1.FieldByname('jumlah').AsString := jumlah.Text;
      ADOTable1.FieldByName('brt').AsString := brt.Text;
      ADOTable1.Post;
      Showmessage('Data Barang Keluar Berhasil Disimpan');
    end;