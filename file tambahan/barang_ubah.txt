if kd_brg.Text='' then
begin
  Showmessage('Kode Barang Masih Kosong');
  kd_brg.SetFocus;
  end else
if nm_brg.Text='' then
begin
  Showmessage('Nama Barang Masih Kosong');
  nm_brg.SetFocus;
  end else
if qty.Text='' then
begin
  Showmessage('Kualitas Masih Kosong');
  qty.SetFocus;
  end else
if satuan.Text='' then
begin
  Showmessage('Satuan Masih Kosong');
  satuan.SetFocus;
  end else
if hrg_satuan.Text='' then
begin
  Showmessage('Harga Satuan Masih Kosong');
  hrg_satuan.SetFocus;
  end else
if hrg_jual.Text='' then
begin
  Showmessage('Harga Jual Masih Kosong');
  satuan.SetFocus;
  end else
if hrg_jual.Text='' then
begin
  Showmessage('Keterangan Masih Kosong');
  ket.SetFocus;
  end else
    begin
	  ADOTable1.Edit;
      ADOTable1.FieldByname('kd_brg').AsString := kd_brg.Text;
      ADOTable1.FieldByName('nm_brg').AsString := nm_brg.Text;
      ADOTable1.FieldByName('qty').AsString := qty.Text;
      ADOTable1.FieldByname('satuan').AsString := satuan.Text;
      ADOTable1.FieldByName('hrg_satuan').AsString := hrg_satuan.Text;
      ADOTable1.FieldByName('hrg_jual').AsString := hrg_jual.Text;
      ADOTable1.FieldByName('ket').AsString := ket.Text;
      ADOTable1.Post;
      Showmessage('Data Barang Berhasil Diubah');
    end;