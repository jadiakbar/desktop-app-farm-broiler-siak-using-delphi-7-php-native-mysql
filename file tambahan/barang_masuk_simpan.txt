if kd_brg.Text='' then
begin
  Showmessage('Kode Barang Masih Kosong');
  kd_brg.SetFocus;
  end else
if kd_suplier.Text='' then
begin
  Showmessage('Kode Suplier Masih Kosong');
  kd_suplier.SetFocus;
  end else
if kd_gudang.Text='' then
begin
  Showmessage('Kode Gudang Masih Kosong');
  kd_gudang.SetFocus;
  end else
if kd_peg.Text='' then
begin
  Showmessage('Kode Pegawai Masih Kosong');
  kd_peg.SetFocus;
  end else
if nm_brg.Text='' then
begin
  Showmessage('Nama Barang Masih Kosong');
  nm_brg.SetFocus;
  end else
if jmlh.Text='' then
begin
  Showmessage('Jumlah Masuk Masih Kosong');
  jmlh.SetFocus;
  end else
if brt.Text='' then
begin
  Showmessage('Berat Masih Kosong');
  brt.SetFocus;
  end else
if tgl_msk.Text='' then
begin
  Showmessage('Tanggal Masuk Masih Kosong');
  tgl_msk.SetFocus;
  end else
if ket.Text='' then
begin
  Showmessage('Keterangan Masih Kosong');
  ket.SetFocus;
  end else
    begin
	  ADOTable1.Open;
	  ADOTable1.Append;
      ADOTable1.FieldByName('kd_brg').AsString := kd_brg.Text;
      ADOTable1.FieldByName('kd_suplier').AsString := kd_suplier.Text;
      ADOTable1.FieldByName('kd_gudang').AsString := kd_gudang.Text;
      ADOTable1.FieldByname('kd_peg').AsString := kd_peg.Text;
      ADOTable1.FieldByName('nm_brg').AsString := nm_brg.Text;
      ADOTable1.FieldByName('jmlh').AsString := jmlh.Text;
      ADOTable1.FieldByname('brt').AsString := brt.Text;
      ADOTable1.FieldByName('tgl_msk').AsString := tgl_msk.Text;
	    ADOTable1.FieldByName('ket').AsString := ket.Text;
      ADOTable1.Post;
      Showmessage('Data Barang Masuk Berhasil Disimpan');
    end;