unit pegawai;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, Buttons, sSpeedButton, Grids, DBGrids, StdCtrls, sBitBtn, sEdit,
  sLabel, sGroupBox, DB, ADODB, Mask, sMaskEdit, sCustomComboEdit,
  sTooledit, sComboBox;

type
  TFrame2 = class(TFrame)
    sGroupBox1: TsGroupBox;
    sLabelFX1: TsLabelFX;
    sLabelFX2: TsLabelFX;
    sLabelFX3: TsLabelFX;
    sLabelFX4: TsLabelFX;
    sLabelFX5: TsLabelFX;
    sLabelFX6: TsLabelFX;
    sLabelFX7: TsLabelFX;
    sLabelFX8: TsLabelFX;
    kd_peg: TsEdit;
    nm_peg: TsEdit;
    alamat: TsEdit;
    sBitBtn1: TsBitBtn;
    sBitBtn2: TsBitBtn;
    sBitBtn3: TsBitBtn;
    sBitBtn4: TsBitBtn;
    sGroupBox2: TsGroupBox;
    DBGrid1: TDBGrid;
    sGroupBox3: TsGroupBox;
    sSpeedButton1: TsSpeedButton;
    sSpeedButton2: TsSpeedButton;
    sSpeedButton3: TsSpeedButton;
    sSpeedButton4: TsSpeedButton;
    sLabelFX9: TsLabelFX;
    cek: TsEdit;
    sBitBtn5: TsBitBtn;
    sLabelFX10: TsLabelFX;
    ket: TsEdit;
    no_telp: TsEdit;
    ADOConnection1: TADOConnection;
    ADOTable1: TADOTable;
    DataSource1: TDataSource;
    tgl_lhr: TsDateEdit;
    tgl_msk: TsDateEdit;
    jen_kel: TsComboBox;
    bagian: TsComboBox;
    sBitBtn6: TsBitBtn;
    procedure sBitBtn1Click(Sender: TObject);
    procedure sBitBtn2Click(Sender: TObject);
    procedure sBitBtn3Click(Sender: TObject);
    procedure sBitBtn4Click(Sender: TObject);
    procedure sSpeedButton1Click(Sender: TObject);
    procedure sSpeedButton2Click(Sender: TObject);
    procedure sSpeedButton3Click(Sender: TObject);
    procedure sSpeedButton4Click(Sender: TObject);
    procedure sBitBtn5Click(Sender: TObject);
    procedure TBClick(Column: TColumn);
    procedure sBitBtn6Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

uses laporan_pegawai;

{$R *.dfm}

procedure TFrame2.sBitBtn1Click(Sender: TObject);
begin
if kd_peg.Text='' then
begin
  Showmessage('Kode Pegawai Masih Kosong');
  kd_peg.SetFocus;
  end else
if nm_peg.Text='' then
begin
  Showmessage('Nama Pegawai Masih Kosong');
  nm_peg.SetFocus;
  end else
if alamat.Text='' then
begin
  Showmessage('Alamat Masih Kosong');
  alamat.SetFocus;
  end else
if no_telp.Text='' then
begin
  Showmessage('Nomor Telepon Masih Kosong');
  no_telp.SetFocus;
  end else
if tgl_lhr.Text='' then
begin
  Showmessage('Tanggal Lahir Masih Kosong');
  tgl_lhr.SetFocus;
  end else
if jen_kel.Text='' then
begin
  Showmessage('Jenis Kelamin Masih Kosong');
  jen_kel.SetFocus;
  end else
if tgl_msk.Text='' then
begin
  Showmessage('Tanggal Masuk Masih Kosong');
  tgl_msk.SetFocus;
  end else
if bagian.Text='' then
begin
  Showmessage('Bagian Masih Kosong');
  bagian.SetFocus;
  end else
 if ket.Text='' then
begin
  Showmessage('Keterangan Masih Kosong');
  ket.SetFocus;
  end else
    begin
	ADOTable1.Open;
	ADOTable1.Append;
      ADOTable1.FieldByname('kd_peg').AsString := kd_peg.Text;
      ADOTable1.FieldByName('nm_peg').AsString := nm_peg.Text;
      ADOTable1.FieldByName('alamat').AsString := alamat.Text;
      ADOTable1.FieldByname('no_telp').AsString := no_telp.Text;
      ADOTable1.FieldByName('tgl_lhr').AsString := tgl_lhr.Text;
      ADOTable1.FieldByName('jen_kel').AsString := jen_kel.Text;
      ADOTable1.FieldByname('tgl_msk').AsString := tgl_msk.Text;
      ADOTable1.FieldByName('bagian').AsString := bagian.Text;
	  ADOTable1.FieldByName('ket').AsString := ket.Text;
      ADOTable1.Post;
      Showmessage('Data Pegawai Berhasil Disimpan');
    end;
end;

procedure TFrame2.sBitBtn2Click(Sender: TObject);
begin
if kd_peg.Text='' then
begin
  Showmessage('Kode Pegawai Masih Kosong');
  kd_peg.SetFocus;
  end else
if nm_peg.Text='' then
begin
  Showmessage('Nama Pegawai Masih Kosong');
  nm_peg.SetFocus;
  end else
if alamat.Text='' then
begin
  Showmessage('Alamat Masih Kosong');
  alamat.SetFocus;
  end else
if no_telp.Text='' then
begin
  Showmessage('Nomor Telepon Masih Kosong');
  no_telp.SetFocus;
  end else
if tgl_lhr.Text='' then
begin
  Showmessage('Tanggal Lahir Masih Kosong');
  tgl_lhr.SetFocus;
  end else
if jen_kel.Text='' then
begin
  Showmessage('Jenis Kelamin Masih Kosong');
  jen_kel.SetFocus;
  end else
if tgl_msk.Text='' then
begin
  Showmessage('Tanggal Masuk Masih Kosong');
  tgl_msk.SetFocus;
  end else
if bagian.Text='' then
begin
  Showmessage('Bagian Masih Kosong');
  bagian.SetFocus;
  end else
 if ket.Text='' then
begin
  Showmessage('Keterangan Masih Kosong');
  ket.SetFocus;
  end else
    begin
	ADOTable1.Edit;
      ADOTable1.FieldByname('kd_peg').AsString := kd_peg.Text;
      ADOTable1.FieldByName('nm_peg').AsString := nm_peg.Text;
      ADOTable1.FieldByName('alamat').AsString := alamat.Text;
      ADOTable1.FieldByname('no_telp').AsString := no_telp.Text;
      ADOTable1.FieldByName('tgl_lhr').AsString := tgl_lhr.Text;
      ADOTable1.FieldByName('jen_kel').AsString := jen_kel.Text;
      ADOTable1.FieldByname('tgl_msk').AsString := tgl_msk.Text;
      ADOTable1.FieldByName('bagian').AsString := bagian.Text;
	  ADOTable1.FieldByName('ket').AsString := ket.Text;
      ADOTable1.Post;
      Showmessage('Data Pegawai Berhasil Diubah');
    end;
end;

procedure TFrame2.sBitBtn3Click(Sender: TObject);
begin
  if messageDlg('Apakah Kode Pegawai '+ADOTable1.FieldByname('kd_peg').AsString+' Ingin Dihapus?',mtConfirmation,[Mbyes,Mbno],0)=mryes then
  begin
  ADOTable1.Delete;
  end;
end;

procedure TFrame2.sBitBtn4Click(Sender: TObject);
begin
    kd_peg.Text:='';
    nm_peg.Text:='';
    alamat.Text:='';
    no_telp.Text:='';
    tgl_lhr.Text:='';
    jen_kel.Text:='';
    tgl_msk.Text:='';
    bagian.Text:='';
	  ket.Text:='';
end;

procedure TFrame2.sSpeedButton1Click(Sender: TObject);
begin
ADOTable1.First;
end;

procedure TFrame2.sSpeedButton2Click(Sender: TObject);
begin
  ADOTable1.Prior;
  if ADOTable1.BOF then
  ShowMessage('Anda sudah diawal data');
end;

procedure TFrame2.sSpeedButton3Click(Sender: TObject);
begin
  ADOTable1.Next;
  if ADOTable1.EOF then
  ShowMessage('Anda sudah diakhir data');
end;

procedure TFrame2.sSpeedButton4Click(Sender: TObject);
begin
	ADOTable1.Last;
end;

procedure TFrame2.sBitBtn5Click(Sender: TObject);
begin
  If not ADOTable1.locate('kd_peg',cek.text,[]) then
  Showmessage('Kode Pegawai Tidak Ditemukan');
end;

procedure TFrame2.TBClick(Column: TColumn);
begin
  kd_peg.Text := ADOTable1.FieldByname('kd_peg').AsString;
  nm_peg.Text := ADOTable1.FieldByName('nm_peg').AsString;
  alamat.Text := ADOTable1.FieldByName('alamat').AsString;
  no_telp.Text := ADOTable1.FieldByname('no_telp').AsString;
  tgl_lhr.Text := ADOTable1.FieldByname('tgl_lhr').AsString;
  jen_kel.Text := ADOTable1.FieldByname('jen_kel').AsString;
  tgl_msk.Text := ADOTable1.FieldByname('tgl_msk').AsString;
  bagian.Text := ADOTable1.FieldByname('bagian').AsString;
  ket.Text := ADOTable1.FieldByname('ket').AsString;
end;

procedure TFrame2.sBitBtn6Click(Sender: TObject);
begin
  LaporanPegawai.Preview;
end;

end.
