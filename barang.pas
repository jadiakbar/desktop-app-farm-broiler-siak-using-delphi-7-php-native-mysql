unit barang;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, StdCtrls, Buttons, sSpeedButton, Grids, DBGrids, sBitBtn, sEdit,
  sLabel, sGroupBox, DB, ADODB, Mask, sMaskEdit, sCustomComboEdit,
  sTooledit, sComboBox;
  
type
  TFrame3 = class(TFrame)
    sGroupBox2: TsGroupBox;
    sGroupBox1: TsGroupBox;
    sLabelFX1: TsLabelFX;
    sLabelFX2: TsLabelFX;
    sLabelFX3: TsLabelFX;
    sLabelFX4: TsLabelFX;
    sLabelFX5: TsLabelFX;
    kd_brg: TsEdit;
    nm_brg: TsEdit;
    hrg_satuan: TsEdit;
    sBitBtn1: TsBitBtn;
    sBitBtn2: TsBitBtn;
    sBitBtn3: TsBitBtn;
    sBitBtn4: TsBitBtn;
    sGroupBox3: TsGroupBox;
    sSpeedButton1: TsSpeedButton;
    sSpeedButton2: TsSpeedButton;
    sSpeedButton3: TsSpeedButton;
    sSpeedButton4: TsSpeedButton;
    sLabelFX9: TsLabelFX;
    cek: TsEdit;
    sBitBtn5: TsBitBtn;
    ADOConnection1: TADOConnection;
    ADOTable1: TADOTable;
    DataSource1: TDataSource;
    qty: TsComboBox;
    satuan: TsComboBox;
    hrg_jual: TsEdit;
    sLabelFX6: TsLabelFX;
    DBGrid1: TDBGrid;
    sLabelFX7: TsLabelFX;
    ket: TsEdit;
    sBitBtn6: TsBitBtn;
    procedure sBitBtn1Click(Sender: TObject);
    procedure sBitBtn2Click(Sender: TObject);
    procedure sBitBtn3Click(Sender: TObject);
    procedure sBitBtn4Click(Sender: TObject);
    procedure TBClick(Column: TColumn);
    procedure sSpeedButton1Click(Sender: TObject);
    procedure sSpeedButton2Click(Sender: TObject);
    procedure sSpeedButton3Click(Sender: TObject);
    procedure sSpeedButton4Click(Sender: TObject);
    procedure sBitBtn5Click(Sender: TObject);
    procedure sBitBtn6Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

uses laporan_barang;

{$R *.dfm}

procedure TFrame3.sBitBtn1Click(Sender: TObject);
begin
if kd_brg.Text='' then
begin
  Showmessage('Kode Barang Masih Kosong');
  kd_brg.SetFocus;
  end else
if nm_brg.Text='' then
begin
  Showmessage('Nama Barang Masih Kosong');
  nm_brg.SetFocus;
  end else
if qty.Text='' then
begin
  Showmessage('Kualitas Masih Kosong');
  qty.SetFocus;
  end else
if satuan.Text='' then
begin
  Showmessage('Satuan Masih Kosong');
  satuan.SetFocus;
  end else
if hrg_satuan.Text='' then
begin
  Showmessage('Harga Satuan Masih Kosong');
  hrg_satuan.SetFocus;
  end else
if hrg_jual.Text='' then
begin
  Showmessage('Harga Jual Masih Kosong');
  satuan.SetFocus;
  end else
if hrg_jual.Text='' then
begin
  Showmessage('Keterangan Masih Kosong');
  ket.SetFocus;
  end else
    begin
	  ADOTable1.Open;
	  ADOTable1.Append;
      ADOTable1.FieldByname('kd_brg').AsString := kd_brg.Text;
      ADOTable1.FieldByName('nm_brg').AsString := nm_brg.Text;
      ADOTable1.FieldByName('qty').AsString := qty.Text;
      ADOTable1.FieldByname('satuan').AsString := satuan.Text;
      ADOTable1.FieldByName('hrg_satuan').AsString := hrg_satuan.Text;
      ADOTable1.FieldByName('hrg_jual').AsString := hrg_jual.Text;
      ADOTable1.FieldByName('ket').AsString := ket.Text;
      ADOTable1.Post;
      Showmessage('Data Barang Berhasil Disimpan');
    end;
end;

procedure TFrame3.sBitBtn2Click(Sender: TObject);
begin
if kd_brg.Text='' then
begin
  Showmessage('Kode Barang Masih Kosong');
  kd_brg.SetFocus;
  end else
if nm_brg.Text='' then
begin
  Showmessage('Nama Barang Masih Kosong');
  nm_brg.SetFocus;
  end else
if qty.Text='' then
begin
  Showmessage('Kualitas Masih Kosong');
  qty.SetFocus;
  end else
if satuan.Text='' then
begin
  Showmessage('Satuan Masih Kosong');
  satuan.SetFocus;
  end else
if hrg_satuan.Text='' then
begin
  Showmessage('Harga Satuan Masih Kosong');
  hrg_satuan.SetFocus;
  end else
if hrg_jual.Text='' then
begin
  Showmessage('Harga Jual Masih Kosong');
  satuan.SetFocus;
  end else
if hrg_jual.Text='' then
begin
  Showmessage('Keterangan Masih Kosong');
  ket.SetFocus;
  end else
    begin
	  ADOTable1.Edit;
      ADOTable1.FieldByname('kd_brg').AsString := kd_brg.Text;
      ADOTable1.FieldByName('nm_brg').AsString := nm_brg.Text;
      ADOTable1.FieldByName('qty').AsString := qty.Text;
      ADOTable1.FieldByname('satuan').AsString := satuan.Text;
      ADOTable1.FieldByName('hrg_satuan').AsString := hrg_satuan.Text;
      ADOTable1.FieldByName('hrg_jual').AsString := hrg_jual.Text;
      ADOTable1.FieldByName('ket').AsString := ket.Text;
      ADOTable1.Post;
      Showmessage('Data Barang Berhasil Diubah');
    end;
end;

procedure TFrame3.sBitBtn3Click(Sender: TObject);
begin
  if messageDlg('Apakah Kode Barang '+ADOTable1.FieldByname('kd_brg').AsString+' Ingin Dihapus?',mtConfirmation,[Mbyes,Mbno],0)=mryes then
  begin
  ADOTable1.Delete;
  end;
end;

procedure TFrame3.sBitBtn4Click(Sender: TObject);
begin
    kd_brg.Text:='';
    nm_brg.Text:='';
	  qty.Text:='';
    satuan.Text:='';
    hrg_satuan.Text:='';
    hrg_jual.Text:='';
    ket.Text:='';
end;

procedure TFrame3.sSpeedButton1Click(Sender: TObject);
begin
ADOTable1.First;
end;

procedure TFrame3.sSpeedButton2Click(Sender: TObject);
begin
  ADOTable1.Prior;
  if ADOTable1.BOF then
  ShowMessage('Anda sudah diawal data');
end;

procedure TFrame3.sSpeedButton3Click(Sender: TObject);
begin
  ADOTable1.Next;
  if ADOTable1.EOF then
  ShowMessage('Anda sudah diakhir data');
end;

procedure TFrame3.sSpeedButton4Click(Sender: TObject);
begin
	ADOTable1.Last;
end;

procedure TFrame3.sBitBtn5Click(Sender: TObject);
begin
  If not ADOTable1.locate('kd_brg',cek.text,[]) then
  Showmessage('Kode Barang Tidak Ditemukan');
end;

procedure TFrame3.TBClick(Column: TColumn);
begin
  kd_brg.Text := ADOTable1.Fieldbyname('kd_brg').AsString;
  nm_brg.Text := ADOTable1.FieldByName('nm_brg').AsString;
  qty.Text := ADOTable1.FieldByName('qty').AsString;
  satuan.Text := ADOTable1.FieldByName('satuan').AsString;
  hrg_satuan.Text := ADOTable1.FieldByname('hrg_satuan').AsString;
  hrg_jual.Text := ADOTable1.FieldByname('hrg_jual').AsString;
  ket.Text := ADOTable1.FieldByname('ket').AsString;
end;

procedure TFrame3.sBitBtn6Click(Sender: TObject);
begin
  LaporanBarang.Preview;
end;

end.
