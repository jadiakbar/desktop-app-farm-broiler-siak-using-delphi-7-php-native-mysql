unit gudang;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, DB, ADODB, Mask, sMaskEdit, sCustomComboEdit, sTooledit,
  Buttons, sSpeedButton, Grids, DBGrids, StdCtrls, sComboBox, sBitBtn,
  sEdit, sLabel, sGroupBox;

type
  TFrame7 = class(TFrame)
    sGroupBox1: TsGroupBox;
    sLabelFX1: TsLabelFX;
    sLabelFX2: TsLabelFX;
    sLabelFX3: TsLabelFX;
    sLabelFX4: TsLabelFX;
    sLabelFX5: TsLabelFX;
    sLabelFX6: TsLabelFX;
    sLabelFX7: TsLabelFX;
    kd_gdg: TsEdit;
    nm_gdg: TsEdit;
    alamat: TsEdit;
    no_telp: TsEdit;
    sBitBtn1: TsBitBtn;
    sBitBtn2: TsBitBtn;
    sBitBtn3: TsBitBtn;
    sBitBtn4: TsBitBtn;
    sGroupBox2: TsGroupBox;
    DBGrid1: TDBGrid;
    sGroupBox3: TsGroupBox;
    sSpeedButton1: TsSpeedButton;
    sSpeedButton2: TsSpeedButton;
    sSpeedButton3: TsSpeedButton;
    sSpeedButton4: TsSpeedButton;
    sLabelFX9: TsLabelFX;
    cek: TsEdit;
    sBitBtn5: TsBitBtn;
    tgl_gdg: TsDateEdit;
    ADOConnection1: TADOConnection;
    ADOTable1: TADOTable;
    DataSource1: TDataSource;
    wilayah: TsEdit;
    ket: TsEdit;
    sBitBtn6: TsBitBtn;
    procedure sBitBtn1Click(Sender: TObject);
    procedure sBitBtn2Click(Sender: TObject);
    procedure sBitBtn3Click(Sender: TObject);
    procedure sBitBtn4Click(Sender: TObject);
    procedure sSpeedButton1Click(Sender: TObject);
    procedure sSpeedButton2Click(Sender: TObject);
    procedure sSpeedButton3Click(Sender: TObject);
    procedure sSpeedButton4Click(Sender: TObject);
    procedure sBitBtn5Click(Sender: TObject);
    procedure TBClick(Column: TColumn);
    procedure sBitBtn6Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

uses laporan_gudang;

{$R *.dfm}

procedure TFrame7.sBitBtn1Click(Sender: TObject);
begin
if kd_gdg.Text='' then
begin
  Showmessage('Kode Gudang Masih Kosong');
  kd_gdg.SetFocus;
  end else
if nm_gdg.Text='' then
begin
  Showmessage('Nama Gudang Masih Kosong');
  nm_gdg.SetFocus;
  end else
if alamat.Text='' then
begin
  Showmessage('Alamat Masih Kosong');
  alamat.SetFocus;
  end else
if no_telp.Text='' then
begin
  Showmessage('Nomor Telepon Masih Kosong');
  no_telp.SetFocus;
  end else
if tgl_gdg.Text='' then
begin
  Showmessage('Tanggal Gudang Masih Kosong');
  tgl_gdg.SetFocus;
  end else
if ket.Text='' then
begin
  Showmessage('Keterangan Masih Kosong');
  ket.SetFocus;
  end else
    begin
	  ADOTable1.Open;
	  ADOTable1.Append;
      ADOTable1.FieldByname('kd_gdg').AsString := kd_gdg.Text;
      ADOTable1.FieldByName('nm_gdg').AsString := nm_gdg.Text;
      ADOTable1.FieldByName('alamat').AsString := alamat.Text;
      ADOTable1.FieldByname('no_telp').AsString := no_telp.Text;
      ADOTable1.FieldByName('tgl_gdg').AsString := tgl_gdg.Text;
      ADOTable1.FieldByName('wilayah').AsString := wilayah.Text;
      ADOTable1.FieldByname('ket').AsString := ket.Text;
      ADOTable1.Post;
      Showmessage('Data Gudang Berhasil Disimpan');
    end;
end;

procedure TFrame7.sBitBtn2Click(Sender: TObject);
begin
if kd_gdg.Text='' then
begin
  Showmessage('Kode Gudang Masih Kosong');
  kd_gdg.SetFocus;
  end else
if nm_gdg.Text='' then
begin
  Showmessage('Nama Gudang Masih Kosong');
  nm_gdg.SetFocus;
  end else
if alamat.Text='' then
begin
  Showmessage('Alamat Masih Kosong');
  alamat.SetFocus;
  end else
if no_telp.Text='' then
begin
  Showmessage('Nomor Telepon Masih Kosong');
  no_telp.SetFocus;
  end else
if tgl_gdg.Text='' then
begin
  Showmessage('Tanggal Gudang Masih Kosong');
  tgl_gdg.SetFocus;
  end else
if ket.Text='' then
begin
  Showmessage('Keterangan Masih Kosong');
  ket.SetFocus;
  end else
    begin
	  ADOTable1.Edit;
      ADOTable1.FieldByname('kd_gdg').AsString := kd_gdg.Text;
      ADOTable1.FieldByName('nm_gdg').AsString := nm_gdg.Text;
      ADOTable1.FieldByName('alamat').AsString := alamat.Text;
      ADOTable1.FieldByname('no_telp').AsString := no_telp.Text;
      ADOTable1.FieldByName('tgl_gdg').AsString := tgl_gdg.Text;
      ADOTable1.FieldByName('wilayah').AsString := wilayah.Text;
      ADOTable1.FieldByname('ket').AsString := ket.Text;
      ADOTable1.Post;
      Showmessage('Data Gudang Berhasil Diubah');
    end;
end;

procedure TFrame7.sBitBtn3Click(Sender: TObject);
begin
  if messageDlg('Apakah Kode Gudang '+ADOTable1.FieldByname('kd_gdg').AsString+' Ingin Dihapus?',mtConfirmation,[Mbyes,Mbno],0)=mryes then
  begin
  ADOTable1.Delete;
  end;
end;

procedure TFrame7.sBitBtn4Click(Sender: TObject);
begin
    kd_gdg.Text:='';
    nm_gdg.Text:='';
	  alamat.Text:='';
    no_telp.Text:='';
    tgl_gdg.Text:='';
    wilayah.Text:='';
	  ket.Text:='';
end;

procedure TFrame7.sSpeedButton1Click(Sender: TObject);
begin
ADOTable1.First;
end;

procedure TFrame7.sSpeedButton2Click(Sender: TObject);
begin
  ADOTable1.Prior;
  if ADOTable1.BOF then
  ShowMessage('Anda sudah diawal data');
end;

procedure TFrame7.sSpeedButton3Click(Sender: TObject);
begin
  ADOTable1.Next;
  if ADOTable1.EOF then
  ShowMessage('Anda sudah diakhir data');
end;

procedure TFrame7.sSpeedButton4Click(Sender: TObject);
begin
	ADOTable1.Last;
end;

procedure TFrame7.sBitBtn5Click(Sender: TObject);
begin
  If not ADOTable1.locate('kd_gdg',cek.text,[]) then
  Showmessage('Kode Gudang Tidak Ditemukan');
end;

procedure TFrame7.TBClick(Column: TColumn);
begin
  kd_gdg.Text := ADOTable1.FieldByname('kd_gdg').AsString;
  nm_gdg.Text := ADOTable1.FieldByName('nm_gdg').AsString;
  alamat.Text := ADOTable1.FieldByName('alamat').AsString;
  no_telp.Text := ADOTable1.FieldByName('no_telp').AsString;
  tgl_gdg.Text := ADOTable1.FieldByname('tgl_gdg').AsString;
  wilayah.Text := ADOTable1.FieldByname('wilayah').AsString;
  ket.Text := ADOTable1.FieldByname('ket').AsString;
end;

procedure TFrame7.sBitBtn6Click(Sender: TObject);
begin
  LaporanGudang.Preview;
end;

end.
