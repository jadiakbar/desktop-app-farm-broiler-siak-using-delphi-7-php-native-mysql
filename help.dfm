object Frame16: TFrame16
  Left = 0
  Top = 0
  Width = 969
  Height = 313
  TabOrder = 0
  object sLabelFX1: TsLabelFX
    Left = 8
    Top = 8
    Width = 64
    Height = 22
    Caption = 'Tentang'
    ParentFont = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold, fsUnderline]
    Angle = 0
    Shadow.OffsetKeeper.LeftTop = -2
    Shadow.OffsetKeeper.RightBottom = 4
  end
  object sLabelFX2: TsLabelFX
    Left = 280
    Top = 40
    Width = 385
    Height = 19
    Caption = 
      'Untuk bantuan penggunaan Program dapat menghubungi informasi dib' +
      'awah ini :'
    Angle = 0
    Shadow.OffsetKeeper.LeftTop = -2
    Shadow.OffsetKeeper.RightBottom = 4
  end
  object sLabelFX4: TsLabelFX
    Left = 160
    Top = 96
    Width = 34
    Height = 19
    Caption = 'Nama'
    Angle = 0
    Shadow.OffsetKeeper.LeftTop = -2
    Shadow.OffsetKeeper.RightBottom = 4
  end
  object sLabelFX5: TsLabelFX
    Left = 160
    Top = 120
    Width = 96
    Height = 19
    Caption = 'Nomor Handphone'
    Angle = 0
    Shadow.OffsetKeeper.LeftTop = -2
    Shadow.OffsetKeeper.RightBottom = 4
  end
  object sLabelFX8: TsLabelFX
    Left = 272
    Top = 96
    Width = 167
    Height = 19
    Caption = ': Indra Husada Praharta Hasibuan'
    Angle = 0
    Shadow.OffsetKeeper.LeftTop = -2
    Shadow.OffsetKeeper.RightBottom = 4
  end
  object sLabelFX9: TsLabelFX
    Left = 272
    Top = 120
    Width = 90
    Height = 19
    Caption = ': 0812 6888 1989'
    Angle = 0
    Shadow.OffsetKeeper.LeftTop = -2
    Shadow.OffsetKeeper.RightBottom = 4
  end
  object sLabelFX12: TsLabelFX
    Left = 760
    Top = 288
    Width = 207
    Height = 19
    Caption = 'Tertanda Indra Husada Praharta Hasibuan'
    Angle = 0
    Shadow.OffsetKeeper.LeftTop = -2
    Shadow.OffsetKeeper.RightBottom = 4
  end
end
