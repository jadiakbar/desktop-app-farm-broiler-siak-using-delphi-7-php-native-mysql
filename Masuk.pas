unit Masuk;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, DB, ADODB, StdCtrls, Buttons, sBitBtn, sEdit, sLabel;

type
  TFrame13 = class(TFrame)
    sLabelFX1: TsLabelFX;
    sLabelFX2: TsLabelFX;
    sLabelFX3: TsLabelFX;
    sLabelFX4: TsLabelFX;
    sEdit1: TsEdit;
    sEdit2: TsEdit;
    sBitBtn1: TsBitBtn;
    ADOConnection1: TADOConnection;
    ADOQuery1: TADOQuery;
    procedure sBitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

uses agen_penjualan, login;

{$R *.dfm}

procedure TFrame13.sBitBtn1Click(Sender: TObject);
begin
if sEdit1.Text = '' then showmessage('Username masih kosong')
  else if sEdit2.Text = '' then Showmessage('Password masih kosong')
       Else begin
          With AdoQuery1 do
         begin
           Close;
           SQL.Clear;
           SQL.Add('select * from tbl_admin where username=:username and password=:password');
           parameters.ParamByName('username').Value:=sEdit1.Text;
           Parameters.ParamByName('password').Value:=sEdit2.Text;
           Open;
         end;
         if not ADOQuery1.IsEmpty   then
         begin
           if UPPERCASE( ADOQuery1.FieldByName ('level').Value )='kasir' then
           begin
             FarmBroiler_Login.Hide;
             Menu_Kasir.Show;
           end
           else
            FarmBroiler_Login.Close;
        Menu_Kasir.Show;
         end else
         MessageBox(handle,'Password atau user yang dimasukan salah','Error',MB_ICOnerror);
       end;
end;

end.
